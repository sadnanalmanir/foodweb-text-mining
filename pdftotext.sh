#!/bin/bash
input_file_directory=$1
output_file_path=$2
#cd $input_file_directory

#for file in 'dir -d *'
for e in $input_file_directory/*;
 do
  #pdftotext $file ../out/$file.txt
  echo $e

  name=$(echo $e | sed 's/.*\///g' | sed 's/map_//')
  echo name $name


  java -jar ./pdfbox-app-2.0.2.jar ExtractText $e $output_file_path/$name.txt
  #pdftohtml -enc UTF-8 $e $output_file_path/$name.html


  mv "$output_file_path/$name.txt" "`echo "$output_file_path/$name.txt" | sed -e 's/\.pdf//g'`"
done
