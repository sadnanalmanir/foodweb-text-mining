package ca.unbsj.cbakerlab.foodweb;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.log4j.Logger;


public class Utils {

	private static final Logger log = Logger.getLogger(Utils.class);

	public static final String stringOntologyUrl = "http://nlp2rdf.lod2.eu/schema/string/";
	public static final String structuredSentenceOntologyUrl = "http://nlp2rdf.lod2.eu/schema/sso/";
	public static final String scmsUrl = "http://ns.aksw.org/scms/";
	public static final String ebiUrl = "http://www.ebi.ac.uk/";
	public static final String gateUrl = "http://gate.ac.uk/";
	public static final String rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public static final String nerdOntology = "http://nerd.eurecom.fr/ontology#";
	public static final String oliaOntology = "http://purl.org/olia/olia.owl#";
	public static final String oliaOntologyUrl = "http://purl.org/olia/olia.owl";
	public static final String oliaSystemOntology = "http://purl.org/olia/system.owl#";
	public static final String pennOntology = "http://purl.org/olia/penn.owl#";

	public static Map<String,String> initFileIndex(String fileOrDirName) throws IOException, URISyntaxException{

        File fileOrDir = new File(fileOrDirName);
        Map<String,String> corpus = new TreeMap<String,String>();

        if(fileOrDir.isDirectory()){            	
            String[] list = fileOrDir.list();
            List<String> files = Arrays.asList(list);
            Collections.sort(files);            
            
            for(String fileName : files){//System.out.println(fileOrDir.getCanonicalPath()+"\n"+fileOrDir.getAbsoluteFile());
            	if(fileName.endsWith(".xml")){ 
            		String documentId = fileName.replaceFirst("\\.xml", "");
            		URL u = new File(fileOrDirName+"/"+ fileName).toURI().toURL();
                	corpus.put(documentId, u.toString());
            	}  
            	if(fileName.endsWith(".txt")){ 
            		String documentId = fileName.replaceFirst("\\.txt", "");
            		URL u = new File(fileOrDirName+"/"+ fileName).toURI().toURL();
                	corpus.put(documentId, u.toString());
            	} 
            }
        }else{
	        String fileName = fileOrDir.getName().split("/")[fileOrDir.getName().split("/").length-1];
	        URL u = new File(fileOrDirName+"/"+ fileName).toURI().toURL();
        	corpus.put(fileName, u.toString());
        }
        
        return corpus;
	}
	

	
	
	public static Integer columnNumber(String columnName, String[] csvHeaderLine) {
		for (int n = 0; n < csvHeaderLine.length; ++n){			
			//System.out.println(csvHeaderLine[n].replaceAll("\"", "")+"  "+columnName);
			if (csvHeaderLine[n].replaceAll("\"", "").equalsIgnoreCase(columnName))
				return new Integer(n);		}
			
		return null;
	}
	
	
	public static void copyfile(String srFile, String dtFile) {
		try {
			File f1 = new File(srFile);
			File f2 = new File(dtFile);
			InputStream in = new FileInputStream(f1);

			// For Append the file.
			// OutputStream out = new FileOutputStream(f2,true);

			// For Overwrite the file.
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
			System.out.println("File copied.");
		} catch (FileNotFoundException ex) {
			System.out
					.println(ex.getMessage() + " in the specified directory.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	static String getContent(File file) throws IOException{
		InputStream is = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is,"Windows-1252"));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
        }
        String content = sb.toString();
		return content;
	}
	
	public static void saveGateXml(gate.Document document, File outputFile, boolean preservingFormat) throws IOException{
		FileWriter fstream = new FileWriter(outputFile);
		BufferedWriter out = new BufferedWriter(fstream);
		if(preservingFormat){
			out.write(document.toXml(document.getAnnotations(), true));
		} else {
			out.write(document.toXml());
		}						
		out.close();
	}	
		
	private void saveGateXml(Document doc, String fileName) throws IOException{
		// Write output files using the same encoding as the original
	      FileOutputStream fos = new FileOutputStream(new File(fileName));
	      BufferedOutputStream bos = new BufferedOutputStream(fos);
	      OutputStreamWriter out;
	      out = new OutputStreamWriter(bos, "UTF-8");	
	      out.write(doc.toXml());			      
	      out.close();
	}
	
	
	
	public static void saveGateXhtml(gate.Document document, File outputFile) throws IOException{
		String head = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> " +
				"<?xml-stylesheet type=\"text/css\" href=\"gate.css\"?>" +
				"<!DOCTYPE html" +
				"     PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"" +
				"    \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" +
				"<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">" +
				"  <head>" +
				"    <title>"+document.getName()+"</title>" +
				"  </head>" +
				"  <body>" +
				"  <p>";
		String tail = "</p> </body> </html> ";
		FileWriter fstream = new FileWriter(outputFile);
		BufferedWriter out = new BufferedWriter(fstream);
		out.write(head);
		out.write(document.toXml(document.getAnnotations(), true));
		out.write(tail);
		out.close();
	}

	
	public static Map<String, Annotation> createIndexForGateDocument(Document doc){
		Map<String, Annotation> index = new HashMap<String, Annotation>();
		for(Annotation ann : doc.getAnnotations().get("Token")){
			String string = doc.getContent().toString().substring(ann.getStartNode().getOffset().intValue(), ann.getEndNode().getOffset().intValue());
			index.put(string, ann);
		}
		
		return index;
	}
	
	public static String mapAsPrettyString(Map map){
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for(Object e : map.keySet()){
			sb.append(e+":"+map.get(e)+"\n");
		}
		return sb.toString();
	}
	
	public static String stringTableAsString(String[][] table){	
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		for(String[] s : table){
			for(String s2 : s){
				sb.append("'"+s2+"' ");
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public static float Round(float Rval, int Rpl) {
		  float p = (float)Math.pow(10,Rpl);
		  Rval = Rval * p;
		  float tmp = Math.round(Rval);
		  return (float)tmp/p;
	}
	
	
	public static float round(float Rval) {
		  float p = (float)Math.pow(10,6);
		  Rval = Rval * p;
		  float tmp = Math.round(Rval);
		  return (float)tmp/p;
	}
	
	static String replaceCharAt(String s, int pos, char c) {
		//System.out.println(s.substring(0, pos));
		//System.out.println(s.substring(pos + 1));
	    return s.substring(0, pos) + c + s.substring(pos + 1);
	}
	
	static File createFileOrDirectoryIfNotExist(String fileOrDirName){
		File newFileOrDir = null;
		if(fileOrDirName != null){
			try{
				newFileOrDir = new File(fileOrDirName);
				if (!newFileOrDir.exists()) {
					boolean success = (new File(fileOrDirName)).mkdir();
					if (success) {
						System.out.println("File or Directory: " + newFileOrDir + " created");
					}else{
						log.warn("File or Directory is not set.");
					}
				}
			}catch(Exception e ){
				log.warn("File or Directory is not set.");
			}				
		}
		return newFileOrDir;
	}
	
	/**
	 * Complexity : 2n
	 * @param annotations
	 * @return
	 */
	public static List<Annotation> getOrderedAnnotations(AnnotationSet annotations){
		List<Annotation> orderedAnnotation = new LinkedList<Annotation>();
		Map<Long,Annotation> orderedAnnotationMap = new TreeMap<Long,Annotation>();
		for(Annotation ann : annotations){
			orderedAnnotationMap.put(ann.getStartNode().getOffset(), ann);
		}
		for(Entry<Long,Annotation> e : orderedAnnotationMap.entrySet()){
			orderedAnnotation.add(e.getValue());
		}
		return orderedAnnotation;
	}
	
}
