package ca.unbsj.cbakerlab.foodweb;

import gate.AnnotationSet;
import gate.CorpusController;
import gate.Factory;
import gate.Gate;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.persist.PersistenceException;
import gate.util.GateException;
import gate.util.persistence.PersistenceManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;



public class OrganismTaggerWrapper {
	private static final Logger log = Logger.getLogger(OrganismTaggerWrapper.class);

	
	private static CorpusController controller;


	
	static {
		try {
			//Gate.getCreoleRegister().registerDirectories(new File(Gate.getPluginsHome(), "JAPE_Plus").toURI().toURL());

			//controller = (CorpusController) PersistenceManager.loadObjectFromFile(new File("/home/artjomk/bin/GATE-7.0/OrganismTagger/gate/OrganismTagger.xgapp"));
			////!!!controller = (CorpusController) PersistenceManager.loadObjectFromFile(new File("/home/artjomk/workspace2/EcotoxicologyTextMining/OrganismTagger/application.xgapp"));
			//	controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file:///ssd/home/artjomk/bin/GATE-7.1/OrganismTagger/gate/OrganismTagger.xgapp"));
			
			
			//controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file:///home/artjomk/bin/GATE-7.1/plugins/OrganismTagger/application.xgapp"));
			
			///!!! this worked controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file:///home/artjomk/bin/GATE-7.1/plugins/OrganismTagger/applicationNoStrainRecognition.xgapp"));
			
			
			controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file://"+Gate.getPluginsHome()+"/OrganismTagger/application.xgapp"));
			//controller = (CorpusController)PersistenceManager.loadObjectFromUrl(new	URL("file://"+Gate.getPluginsHome()+"/OrganismTagger/applicationNoStrainRecognition.xgapp"));

			
			///controller = (CorpusController) PersistenceManager.loadObjectFromUrl((log.getClass().getResource("/gate/plugins/application1/application.xgapp").toURI().toURL()));
		} catch (PersistenceException e1) {
			e1.printStackTrace();
		} catch (ResourceInstantiationException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		//} catch (URISyntaxException e) {
		//	e.printStackTrace();
		} catch (GateException e) {
			e.printStackTrace();
		}
	}
	
	
	private static gate.Document process(String text) throws ResourceInstantiationException, ExecutionException{
		gate.Document doc = Factory.newDocument(text);    	
		gate.Corpus gateCorpus = Factory.newCorpus("corpus"+doc.getName());    			
        gateCorpus.add(doc);
        synchronized(controller){
        	controller.setCorpus(gateCorpus);   
        	controller.execute();		
        	Factory.deleteResource(gateCorpus);	
        }
		return doc;
	}
	
	
	public static void annotate(gate.Document doc) throws ResourceInstantiationException, ExecutionException {		
			String text = doc.getContent().toString();		
			gate.Document annotatedDoc = process(text);
			AnnotationSet organismAnnotationSet  = annotatedDoc.getAnnotations().get("Organism");	
			//System.err.println(organismAnnotationSet.size());
			//for(Annotation ann : organismAnnotationSet){
			//	System.err.println(ann);
			//	doc.getAnnotations().add(ann);
			//}
			/*
        	try {
				Utils.saveGateXml(doc, new File(doc.getName()+".xml"), false);
			} catch (IOException e) {
				e.printStackTrace();
			}
        	try {
				Utils.saveGateXml(annotatedDoc, new File(doc.getName()+"2.xml"), false);
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			doc.getAnnotations()
			.addAll(organismAnnotationSet);				
	}
	
	
	public static void main(String[] args) throws Exception {

		// String text = "Should it be used with Indinavir? I worked at an HIV clinic for two years. We used St. John's with great results right alongside conventional anti-virals with no adverse interaction. The major side-effects of Indinavir are headaches, nausea, vomiting, diarrhea, dizziness and insomnia. Again, after consulting with your physician, you may consider trying a good quality St. John's Wort with regular monitoring. What about Digoxin and St. John's? This is probably referring to a small (162) study of in which St. John's demonstrated its ability to promote stable heart function while decreasing depression. So, yes, it's an herb trying to stabilize the organism and may tonify the heart. This is a beneficial side effect. Why is Theophylline, primarily an asthmatic drug, listed? Probably for the same reason as digoxin. Theophylline can cause life-threatening ventricular arrythmias. (And frankly, don't use St. John's when using Theophylline because if the ventricular arrythmias occur, St. John's will be blamed.) Should St. John's be used with immune suppressants such as Cyclosporin? Absolutely not. The overwhelming majority of medicinal herbs should be avoided with immune suppressants. Herbs strengthen the human organism. They would work directly against drugs designed to weaken the human organism. What about using St. John's with chemotherapy? In my practice, I tend to avoid most herbs during the actual process of chemotherapy. I use them before hand to build up the body and afterward to repair the damage to the normal cells. Periodically, the pharmaceutical industry feels threatened by a natural product and goes after it in a very deliberate way. About 10 years ago, it was L-Tryptophan. L-Tryptophan was an inexpensive amino acid that was extremely effective in increasing seratonin levels and was virtually without side effects. A Japanese company produced a single bad batch after failing to complete the chemical process and several deaths occurred. The FDA was able to trace the bad batch back to the Japanese company, but still decided to ban the supplement. Two years ago, the media flooded the public with warnings that St. John's Wort would cause birth control pills to fail resulting in a host of unwanted pregnancies. When the pregnancies didn't occur, the pharmaceuticals tried a different angle. If it were patentable and the pharmaceutical companies could make money off of it, this Internet email warning about herbs would not have occurred. Consider Viagra. It has caused over 60 deaths and continues to be on the market.";
		// String text = "Concentrations of the alcohols, especially of the higher-moiecular-weight ones, are the rea- sons why GC methods cannot be applied satisfactorily to the quantification of endogenous a ! cohols . In this paper two methods are described for the quantitative determination of endogeaous ethanol and endogenous higher-molecular-wei  t cohols . taking advantage of the high specificity and sensitivity of mass fra mento- graphy ( MF ). Serum and urine samp ! es were collected from norrnal individuals , from hos- pttal patients without obvious metabolic defects and from diabetic patients who had abstained from drinking alcoholic beverages for 3 days before sample collection . Serum was obtained from venous blood by centrifugation for 10 min at 1600 g . Urine The GC-MF determinations were performed on a combination of a lMode1 2700 gas chromatograph and a CH 5 mass spectrometer ( Varian-MAT , Bremen . Ethanol was determined by direct injection of a serum or urine sample . To 0 . 5 ml of serum or urine 2  1 of internal standard ( 50 J . LI of diethyl ether in 100 ml of distilled water ) were added . The mixuxe was thoro ly shaken and I {} 1 The water from the directly injected serum or urine sample was by-passed between the outlet of the GC column and the interface ";
		///String text = "Expired air samples have been analyzed from three groups of human subjects (normal, liver dysfunction, lung cancer) and the baboon (Papio anubus). Of the several hundred compounds present, three compounds were of particular interest due to their structural relationship to the isoprenoid-type intermediates in the sterol pathway. These compounds were 1-methyl-4-(1-methyl-ethenyl)-cyclohexene, 6-methyl-5-hepten-2-one, and 6,10-dimethyl-5,9-undecadien-2-one. Hydroxyacetone was also found in all samples screened. The relationship of these compounds to the non-sterol pathway of mevalonate metabolism is discussed.";
		String text = "Daphnia magna is cool. Dogs are also cool./n";
		
			
		//
		// Load properties. (home for GATE).
		//
		Properties pro = new Properties();
		try {
			pro.load(new FileInputStream(new File(log.getClass().getClassLoader().getResource("project.properties").toURI())));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		//
		// Load GATE.
		//
		String gateHome = pro.getProperty("GATE_HOME");
		if (!Gate.isInitialised()) {
			log.info("Initializing GATE...");

			// Set GATE HOME directory.
			Gate.setGateHome(new File(gateHome));
			// Set GATE HOME directory.
			Gate.setPluginsHome(new File(gateHome, "plugins"));
			log.info(Gate.getPluginsHome());
			// Initialise GATE.
		//	Gate.runInSandbox(true);
			Gate.init();
			log.info("Initializing GATE... Done.");
		}

		
		//
		// Init OrganismTaggerWrapper after Gate is initialized.
		//
		OrganismTaggerWrapper ow = new OrganismTaggerWrapper();

		
		//
		// Create gate document from input text.
		//
		gate.Document targetDocument = null;
		try {
			targetDocument = Factory.newDocument(text);
		} catch (ResourceInstantiationException e1) {
			e1.printStackTrace();
		}

		//
		// Transfer annotations from whatizit output to original document.
		//
		ow.annotate(targetDocument);

		Utils.saveGateXml(targetDocument, new File("temp2.xml"), false);

		
		System.out.println("Finished.");
	}
}
