package ca.unbsj.cbakerlab.foodweb;

import gate.Corpus;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;

/**
 *
 * @author UNBSJ
 */
public class Pipeline implements Serializable {

    private static final Logger log = Logger.getLogger(Pipeline.class);

    private SerialAnalyserController annieController;

    public Pipeline(String gateHome) throws GateException {
        if (!Gate.isInitialised()) {
            log.info("Initializing GATE...");

            // Set GATE HOME directory.
            Gate.setGateHome(new File(gateHome));
            // Set GATE HOME directory.
            Gate.setPluginsHome(new File(gateHome, "plugins"));
            System.out.println(Gate.getPluginsHome());
            // Initialise GATE.
            Gate.init();
            log.info("Initializing GATE... Done.");
        }
    }

    public void init(boolean runDocumentResetter,
            boolean runTokenizer,
            boolean runSentenceSplitter,
            boolean runOrganismTagger,
            boolean runNpChunker,
            boolean runChemicalExtractor,
            boolean runDiseaseExtractor,
            boolean runCooccurrenceExtractor
    ) throws GateException, URISyntaxException, FileNotFoundException, IOException {

        // Load ANNIE plugin.
        File pluginsHome = Gate.getPluginsHome();
        Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "ANNIE").toURI().toURL());

        // Create a serial analyser controller to run ANNIE with.
        annieController = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController",
                Factory.newFeatureMap(), Factory.newFeatureMap(), "ANNIE_" + Gate.genSym());

        if (runDocumentResetter) {
            // Add tokenizer.
            FeatureMap prParams = Factory.newFeatureMap();
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR", prParams);
            annieController.add(pr);
            log.info("document resetter added.");
        }

        if (runTokenizer) {
            // Add tokenizer.
            FeatureMap tokParams = Factory.newFeatureMap();
            ProcessingResource tokPr = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser", tokParams);
            annieController.add(tokPr);
            log.info("tokenizer added");
        }

        if (runSentenceSplitter) {
            // Add sentence splitter.
            FeatureMap senParams = Factory.newFeatureMap();
            ProcessingResource senPr = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter", senParams);
            annieController.add(senPr);
            log.info("sentence splitter added");
        }

        if (runOrganismTagger) {
            log.info("organism tagger adding...");
            Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "JAPE_Plus").toURI().toURL());
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/OrganismTagger.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("organism tagger added");
        }

        {
            log.info("adding foodweb gazetteer...");
            FeatureMap owParams = Factory.newFeatureMap();
            owParams.put("caseSensitive", false);
            owParams.put("encoding", "UTF-8");
            owParams.put("gazetteerFeatureSeparator", "@");
            owParams.put("listsURL", this.getClass().getClassLoader().getResource("gate/gazetteers/foodweb.def").toURI().toURL());
            owParams.put("longestMatchOnly", true);
            ProcessingResource owPr = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer", owParams);
            annieController.add(owPr);
            log.info("foodweb gazetteer added");
        }

        {
            log.info("Lookup transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/LookupTransducer.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("Lookup transducer added");
        }

        {
            log.info("Number tagger adding...");
            Gate.getCreoleRegister().registerDirectories(new File(pluginsHome, "Tagger_Numbers").toURI().toURL());
            ProcessingResource posPr = (ProcessingResource) Factory.createResource("gate.creole.numbers.NumbersTagger", Factory.newFeatureMap());
            annieController.add(posPr);
            log.info("Number tagger added");
        }

        {
            log.info("LargeNumberRemover transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/LargeNumberRemover.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("LargeNumberRemover transducer added");
        }

        {
            log.info("BodySizeValueDeterminer transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/BodySizeValueDeterminer.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("BodySizeValueDeterminer transducer added");
        }

        /*
         {
         log.info("DurationExtractor transducer adding...");
         FeatureMap params = Factory.newFeatureMap();
         params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/DurationExtractor.jape").toURI().toURL());
         ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
         annieController.add(pr);
         log.info("DurationExtractor transducer added");
         }
         */
        /* might be needed in future
         {
         log.info("abbreviation resolver transducer adding...");
         FeatureMap params = Factory.newFeatureMap();
         params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/AbbreviationResolver.jape").toURI().toURL());
         ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
         annieController.add(pr);
         log.info("abbreviation resolver transducer added");
         }
         */
        /*
         {
         log.info("SectionIdentifier transducer adding...");
         FeatureMap params = Factory.newFeatureMap();
         params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/SectionIdentifier.jape").toURI().toURL());
         ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
         annieController.add(pr);
         log.info("SectionIdentifier transducer added");
         }
         */
        /*
         {
         log.info("AbstractSectionExtractor transducer adding...");
         FeatureMap params = Factory.newFeatureMap();
         params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/AbstractSectionExtractor.jape").toURI().toURL());
         ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
         annieController.add(pr);
         log.info("AbstractSectionExtractor transducer added");
         }
        
         {
         log.info("MethodSectionExtractor transducer adding...");
         FeatureMap params = Factory.newFeatureMap();
         params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/MethodSectionExtractor.jape").toURI().toURL());
         ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
         annieController.add(pr);
         log.info("MethodSectionExtractor transducer added");
         }

         {
         log.info("ResultSectionExtractor transducer adding...");
         FeatureMap params = Factory.newFeatureMap();
         params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/ResultSectionExtractor.jape").toURI().toURL());
         ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
         annieController.add(pr);
         log.info("ResultSectionExtractor transducer added");
         }
         */
        if (runCooccurrenceExtractor) {
            log.info("cooccurrence transducer adding...");
            FeatureMap params = Factory.newFeatureMap();
            params.put("grammarURL", this.getClass().getClassLoader().getResource("gate/japerules/CooccurrenceMatcher.jape").toURI().toURL());
            ProcessingResource pr = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer", params);
            annieController.add(pr);
            log.info("cooccurrence transducer added");
        }

    }

    /**
     * Tell MutationGrounderApp's controller about the corpus you want to run on
     */
    public void setCorpus(Corpus corpus) {
        annieController.setCorpus(corpus);
    }

    /**
     * Run MutationGrounderApp
     */
    public void execute(gate.Document doc) throws GateException {
        log.info("Running Pipeline...");
        gate.Corpus gateCorpus = Factory.newCorpus("corpus" + doc.getName());
        gateCorpus.add(doc);
        this.setCorpus(gateCorpus);
        annieController.execute();

        Factory.deleteResource(gateCorpus);

        log.info("...Pipeline complete");
    }

}
