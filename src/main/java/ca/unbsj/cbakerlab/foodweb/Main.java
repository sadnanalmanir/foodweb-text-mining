package ca.unbsj.cbakerlab.foodweb;

import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.relations.Relation;
import gate.relations.RelationSet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;

public class Main {

    private static final Logger log = Logger.getLogger(Main.class);

    static long corpusQueryTime = 0;

    //
    // Statistics.
    //
    public static int numberGoldStandardRecords = 0;
    public static int numberRelMatched = 0;
    public static int numberRelNotMatched = 0;
    public static int numberDocsNotMatched = 0;
    public static int numberDocsCompleteMatched = 0;
    public static int numberDocsPartiallyMatched = 0;

    public Main() {

    }

    /**
     * Executed from main. Supports JSAP parser to read arguments from command
     * line. Details of how to use arguments in Main please read in readme
     *
     * @param args arguments from command line
     * @throws Exception
     */
    public static void run(String args) {

        try {
            long time = System.currentTimeMillis();

            // Get command line arguments.
            boolean debug = false;

            String inputFileOrDirName = null;
            String xmlOutputDirName = null;

            String outputResultsFileName = null;
            String goldStandardFileName = null;

            boolean runPipe = false;
            boolean runStatistics = false;

            //
            // Define options.
            //
            {
                JSAP jsap = new JSAP();
                {
                    FlaggedOption s = new FlaggedOption("input").setStringParser(JSAP.STRING_PARSER).setLongFlag("input").setShortFlag('i');
                    s.setHelp("The name of input file or directory.");
                    jsap.registerParameter(s);
                }
                {
                    FlaggedOption s = new FlaggedOption("xmldir").setStringParser(JSAP.STRING_PARSER).setLongFlag("xmldir").setShortFlag('x');
                    s.setHelp("The name of xml file or directory.");
                    jsap.registerParameter(s);
                }
                {
                    FlaggedOption s = new FlaggedOption("outputResults").setStringParser(JSAP.STRING_PARSER).setLongFlag("output").setShortFlag('o');
                    s.setHelp("Name of the file for results.");
                    jsap.registerParameter(s);
                }
                {
                    FlaggedOption s = new FlaggedOption("goldStandardOutput").setStringParser(JSAP.STRING_PARSER).setLongFlag("gsOutput").setShortFlag('g');
                    s.setHelp("Name of the file for results.");
                    jsap.registerParameter(s);
                }

                {
                    Switch s = new Switch("debugging").setLongFlag("debug").setShortFlag('d');
                    s.setHelp("Enable debugging messages");
                    jsap.registerParameter(s);
                }

		//
                // Execute program.
                //
                {
                    Switch s = new Switch("runDocumentResetter").setLongFlag("runDocumentResetter").setLongFlag("reset");
                    s.setHelp("runDocumentResetter");
                    jsap.registerParameter(s);
                }

                {
                    Switch s = new Switch("runPipe").setLongFlag("pipe");
                    s.setHelp("runPipe");
                    jsap.registerParameter(s);
                }

                {
                    Switch s = new Switch("runStatistics").setLongFlag("runStatistics");
                    s.setHelp("runStatistics");
                    jsap.registerParameter(s);
                }

                // Parse the command-line arguments.
                JSAPResult config = jsap.parse(args);

                // Display the helpful help in case trouble happens.
                if (!config.success()) {
                    System.err.println();
                    System.err.println(" " + jsap.getUsage());
                    System.err.println();
                    System.err.println(jsap.getHelp());
                    System.exit(1);
                }

		// Assign to variables.
		//
                // Directories.
                //
                inputFileOrDirName = config.getString("input");
                if (inputFileOrDirName != null) {
                    log.info("corpus path: " + inputFileOrDirName);
                }

                xmlOutputDirName = config.getString("xmldir");
                if (xmlOutputDirName != null) {
                    log.info("xml output path: " + xmlOutputDirName);
                }

		//
                // Modules.
                //
                runPipe = config.getBoolean("runPipe");
                if (runPipe) {
                    log.info("runPipe: " + runPipe);
                }

                runStatistics = config.getBoolean("runStatistics");
                if (runStatistics) {
                    log.info("runStatistics: " + runStatistics);
                }

		//
                // Resources.
                //
                outputResultsFileName = config.getString("outputResults");
                if (outputResultsFileName != null) {
                    log.info("results File path: " + outputResultsFileName);
                }

                goldStandardFileName = config.getString("goldStandardOutput");
                if (goldStandardFileName != null) {
                    log.info("goldStandardOutput path: " + goldStandardFileName);
                }

                debug = config.getBoolean("debugging");
                log.info("debug: " + debug);
            }

            // ** set up logging.
            if (debug == true) {
                PropertyConfigurator.configure(log.getClass().getClassLoader().getResource("log4j.properties"));
                java.util.logging.Logger logg = java.util.logging.Logger.getLogger("JAPELogger");
                logg.setLevel(Level.OFF);
                logg.log(Level.INFO, "LOG INIT");
            } else {
                PropertyConfigurator.configure(new File("resources/log4jOff.properties").toURI().toURL());
                java.util.logging.Logger logg = java.util.logging.Logger.getLogger("JAPELogger");
                logg.setLevel(Level.OFF);
                logg.log(Level.INFO, "LOG INIT");
            }

            // Create xml output directory if it does not exist.
            File xmlOutputDir = null;
            if (xmlOutputDirName != null) {
                xmlOutputDir = Utils.createFileOrDirectoryIfNotExist(xmlOutputDirName);
            }

            //
            // RUN PIPELINE.
            //	
            if (runPipe) {

                Properties pro = new Properties();
                try {
                    pro.load(new FileInputStream(new File(log.getClass().getClassLoader().getResource("project.properties").toURI())));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                String gateHome = pro.getProperty("GATE_HOME");
		//
                // Init pipeline.
                //
                Pipeline pipeline = new Pipeline(gateHome);
                pipeline.init(
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true
                );
		//
                // Prepare and Process corpus.
                //        
                Map<String, String> fileIndex = Utils.initFileIndex(inputFileOrDirName);

                // Monitor which file is currently processed.
                int numberOfFilesToProcess = fileIndex.size();
                if (processOnlyDocuments.size() > 1) {
                    numberOfFilesToProcess = processOnlyDocuments.size() - 1;
                }
                int numberOfFileProcessed = 0;

                for (String fileName : fileIndex.keySet()) {

                    if (processOnlyDocuments.size() > 1 && !processOnlyDocuments.contains(fileName)) {
                        continue;
                    }

                    numberOfFileProcessed++;
                    log.info("\n=================================================================\n"
                            + "DOCUMENT: " + fileName + "		" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date())
                            + "\t" + numberOfFileProcessed + " of " + numberOfFilesToProcess
                            + "\n=================================================================\n");

                    gate.Document doc = Factory.newDocument(new URL(fileIndex.get(fileName)));
                    doc.setName(fileName);

		    //log.info(doc.getContent());
                    pipeline.execute(doc);

                    if (xmlOutputDir != null) {
                        Utils.saveGateXml(doc, new File(xmlOutputDir + "/" + doc.getName() + ".xml"), false);
                    }

                  /*  RelationSet relSet = RelationSet.getRelations(doc.getAnnotations());
                    for (Relation r : relSet.getRelations("ToxicityInfo")) {
                        System.out.println(((FeatureMap) r.getUserData()).get("chemical"));
                        System.out.println(((FeatureMap) r.getUserData()));
                    }
                  */
                    Factory.deleteResource(doc);

                    log.info("Processing Document Time " + (System.currentTimeMillis() - time) / 1000 + " seconds = " + (System.currentTimeMillis() - time) / 1000 / 60 + " minutes");
                    log.info("Query Document Time " + corpusQueryTime / 1000 + " seconds");
                }

                log.info("Processing Corpus Time " + (System.currentTimeMillis() - time) / 1000 + " seconds");
            }

            if (runStatistics) {
                boolean append = false;
                File outputFile = new File(outputResultsFileName);

                Properties pro = new Properties();
                try {
                    pro.load(new FileInputStream(new File(log.getClass().getClassLoader().getResource("project.properties").toURI())));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                //pro.load(new FileInputStream(new File(ClassLoader.getSystemClassLoader().getResource("project.properties").toURI())));

                String gateHome = pro.getProperty("GATE_HOME");
                if (!Gate.isInitialised()) {
                    log.info("Initializing GATE...");

                    // Set GATE HOME directory.
                    Gate.setGateHome(new File(gateHome));
                    // Set GATE HOME directory.
                    Gate.setPluginsHome(new File(gateHome, "plugins"));
                    System.out.println(Gate.getPluginsHome());
                    // Initialise GATE.
                    Gate.init();
                    log.info("Initializing GATE... Done.");
                }

		        //
                // Prepare and Process corpus.
                //        
                Map<String, String> fileIndex = Utils.initFileIndex(xmlOutputDirName);
                

                // Monitor which file is currently processed.
                int numberOfFilesToProcess = fileIndex.size();
                if (processOnlyDocuments.size() > 1) {
                    numberOfFilesToProcess = processOnlyDocuments.size() - 1;
                }
                int numberOfFileProcessed = 0;

                BufferedWriter statsBuffWriter = null;
                statsBuffWriter = new BufferedWriter(new FileWriter(outputFile, false));
                /*
                statsBuffWriter.write("document" + "\t"
                        + "org_mention" + "\t"
                        + "org_id" + "\t"
                        + "org_text" + "\t"
                        + "chem_mention" + "\t"
                        + "chem_id" + "\t"
                        + "chem_text" + "\t"
                        + "ChOTDC_1" + "\t"
                        + "ChOTDC_2" + "\t"
                        + "ChOTDC_3" + "\t"
                        + "ChTDC_1" + "\t"
                        + "ChTDC_2" + "\t"
                        + "ChTDC_3" + "\t"
                        + "OTDC_1" + "\t"
                        + "OTDC_2" + "\t"
                        + "OTDC_3" + "\t"
                        + "TDC_1" + "\t"
                        + "TDC_2" + "\t"
                        + "TDC_3" + "\n"
                        + "ToxicityInfo" + "\n");
                */
                statsBuffWriter.write("document" + "\t"
                        //+ "org_mention" + "\t"
                        //+ "org_id" + "\t"
                        //+ "org_text" + "\t"
                        //+ "chem_mention" + "\t"
                        //+ "chem_id" + "\t"
                        //+ "chem_text" + "\t"
                        //+ "Organism" + "\t"
                        //+ "BodySizeType" + "\t"
                        //+ "BodySizeValue" + "\t"
                        //+ "Gender" + "\t"
                        + "Coo_BSTOrMuBSV_1" + "\t"
                        + "Coo_BSTOrMuBSV_2" + "\t"
                        + "Coo_BSTOrMuBSV_3" + "\t"
                        + "Coo_BSTOrNumMuBSV_1" + "\t"
                        + "Coo_BSTOrNumMuBSV_2" + "\t"
                        + "Coo_BSTOrNumMuBSV_3" + "\t"
                        //+ "TDC_2" + "\t"
                        //+ "TDC_3" + "\n"
                        //+ "ToxicityInfo" + "\n");
                        + "\n");
                BufferedWriter annBuffWriter = new BufferedWriter(new FileWriter("annotations.csv", false));
                annBuffWriter.write("document" + "\t"
                                    + "BST=BodySizeType, Or=Organism, Num=Number, Mu=MeasurementUnit, BSV=BodySizeValue)" + "\t" 
                                    + "Sentences with (co)occurrence(s)" + "\n");
                for (String fileName : fileIndex.keySet()) {

                    if (processOnlyDocuments.size() > 1 && !processOnlyDocuments.contains(fileName)) {
                        continue;
                    }

                    numberOfFileProcessed++;
                    log.info("\n=================================================================\n"
                            + "DOCUMENT: " + fileName + "		" + new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date())
                            + "\t" + numberOfFileProcessed + " of " + numberOfFilesToProcess
                            + "\n=================================================================\n");

                    gate.Document doc = Factory.newDocument(new URL(fileIndex.get(fileName)));
                    doc.setName(fileName);

		        	//
                    // STATS.
                    //
                    //Integer[] stats1 = Statistics.calculateStatsForDoc(doc, "Organism", "ann", "ncbiId", null);
                    //Integer[] stats2 = Statistics.calculateStatsForDoc(doc, "Oscar4_CM", "ann", "InChI", null);
                    Integer[] stats3 = Statistics.calculateStatsForDoc(doc, "Coo_BSTOrMuBSV_1_sent", "ann", null, annBuffWriter);
                    Integer[] stats4 = Statistics.calculateStatsForDoc(doc, "Coo_BSTOrMuBSV_2_sent", "ann", null, annBuffWriter);
                    Integer[] stats5 = Statistics.calculateStatsForDoc(doc, "Coo_BSTOrMuBSV_3_sent", "ann", null, annBuffWriter);
                    Integer[] stats6 = Statistics.calculateStatsForDoc(doc, "Coo_BSTOrNumMuBSV_1_sent", "ann", null, annBuffWriter);
                    Integer[] stats7 = Statistics.calculateStatsForDoc(doc, "Coo_BSTOrNumMuBSV_2_sent", "ann", null, annBuffWriter);
                    Integer[] stats8 = Statistics.calculateStatsForDoc(doc, "Coo_BSTOrNumMuBSV_3_sent", "ann", null, annBuffWriter);
                    //Integer[] stats9 = Statistics.calculateStatsForDoc(doc, "Coo_BOrGLsMw_1_sent", "ann", null, annBuffWriter);
                    //Integer[] stats10 = Statistics.calculateStatsForDoc(doc, "Coo_BOrGLsMwMu_1_sent", "ann", null, annBuffWriter);
                    //Integer[] stats11 = Statistics.calculateStatsForDoc(doc, "Methods", "ann", null, annBuffWriter);
                    //Integer[] stats12 = Statistics.calculateStatsForDoc(doc, "Results", "ann", null, annBuffWriter);
                    //Integer[] stats13 = Statistics.calculateStatsForDoc(doc, "Results", "ann", null, annBuffWriter);
                    //Integer[] stats14 = Statistics.calculateStatsForDoc(doc, "Methods", "ann", null, annBuffWriter);
                    //Integer[] stats15 = Statistics.calculateStatsForDoc(doc, "Results", "ann", null, annBuffWriter);
                    //Integer[] stats15 = Statistics.calculateStatsForDoc(doc, "ToxicityInfo", "rel", null, annBuffWriter);
                    //log.debug(Arrays.asList(stats1));
                    //log.debug(Arrays.asList(stats2));
                    //log.debug(Arrays.asList(stats3));
                    //log.debug(Arrays.asList(stats4));
                    //log.debug(Arrays.asList(stats9));

                    String newRow = doc.getName() + "\t"
                            //+ stats1[0] + "\t" + stats1[1] + "\t" + stats1[2] + "\t"
                            //+ stats2[0] + "\t" + stats2[1] + "\t" + stats2[2] + "\t"
                            + stats3[0] + "\t"
                            + stats4[0] + "\t"
                            + stats5[0] + "\t"
                            + stats6[0] + "\t"
                            + stats7[0] + "\t"
                            + stats8[0] + "\t"
                            //+ stats9[0] + "\t"
                            //+ stats10[0] + "\t"
                            //+ stats11[0] + "\t"
                            //+ stats12[0] + "\t"
                            //+ stats13[0] + "\t"
                            //+ +stats14[0] + "\t"
                            //+ +stats15[0] + "\t"
                            + "\n";

                    String modNewRow = newRow.replaceAll("\t0", "\t ");

                    System.err.println(newRow);
                    System.err.println(modNewRow);

                    statsBuffWriter.write(modNewRow);

                    Factory.deleteResource(doc);

                    log.info("Processing Document Time " + (System.currentTimeMillis() - time) / 1000 + " seconds = " + (System.currentTimeMillis() - time) / 1000 / 60 + " minutes");
                    log.info("Query Document Time " + corpusQueryTime / 1000 + " seconds");
                }
                statsBuffWriter.close();
                annBuffWriter.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static String norm(int i) {
        if (i == 0) {
            return "";
        } else {
            return Integer.toString(i);
        }
    }

    static Set<String> processOnlyDocuments = new HashSet<String>(Arrays.asList(
            "x"
    ));

    static Set<String> doNotProcessOnlyDocuments = new HashSet<String>(Arrays.asList(
            "x"));

    public static void main(String[] args) throws Exception {

        boolean hasArguments = false;
        if (args.length != 0) {
            if (args.length == 1 && args[0].equals("${args}")) {
                hasArguments = false;
            } else {
                hasArguments = true;
            }
        }

        if (hasArguments) {
            String arguments = Arrays.toString(args).replace(", ", " ");
            arguments = arguments.substring(1, arguments.length() - 1);
            log.info("ARGUMENTS: " + arguments);
            run(arguments);
        } else {
            System.out.println("NO ARGUMENTS. CHECKARGUMENTS.");

        }
        System.out.println("\nAll done.");

    }
}
