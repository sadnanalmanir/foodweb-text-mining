package ca.unbsj.cbakerlab.foodweb;

import gate.Annotation;
import gate.FeatureMap;
import gate.relations.Relation;
import gate.relations.RelationSet;
import gate.util.InvalidOffsetException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

public class Statistics {
	private static final Logger log = Logger.getLogger(Statistics.class);
	
	// private static Map<String,int[]> statistics = new LinkedHashMap<String,int[]>();
	 
	// public Map<String,int[]> getStatistics(){
	//	 return statistics;
	 //}
	 
	
	 
	 
	 /**
	  * 
	  * @param doc
	  * @param type - annotation types to calculate statistics for
	  * @param feature - feature to print
	  * @param print - print annotation into table pf annotations (annotation.csv)
	  * @return
	  * @throws IOException
	  */
	public static Integer[] calculateStatsForDoc(gate.Document doc, String type, String annotationOrRelation, String feature, BufferedWriter textBuffWriter) throws IOException {
		// 1 - number of annotations
		// 2 - number of feature
		// 3 - number of context
		Integer[] stats = { 0, 0, 0 };
		Set<String> surfaceStrings = new HashSet<String>();
		Set<String> featureValues = new HashSet<String>();

		// IF GATE ANNOTATION
		if(annotationOrRelation.equalsIgnoreCase("ann")){
			Set<Annotation> annMentions = doc.getAnnotations().get(type);
			stats[0] = annMentions.size();
                        System.out.println("Annotation of type == "+ type +" is :" + annMentions.size());
			for (Annotation ann : annMentions) {
				try {
					String annString = doc.getContent().getContent(ann.getStartNode().getOffset(), ann.getEndNode().getOffset()).toString().replace("\n"," ").trim();
                                        //System.out.println("annString : "+ annString);
					if(annString != null && !annString.equals("") && !annString.equals(" "))
						surfaceStrings.add(annString);
									
					
					if(feature != null){
						String featureValue = (String)ann.getFeatures().get(feature);
						if(featureValue != null && !featureValue.equals("") && !featureValue.equals(" "))
							featureValues.add(featureValue);	
					}else{					
						featureValues.add(annString);
					}
								
					
					
					if (textBuffWriter != null){
                                            //System.out.println("not null");
						textBuffWriter.write(doc.getName()+"\t"+ann.getType()+"\t"+annString+"\n");
					}
					
					
				} catch (InvalidOffsetException e) {
					e.printStackTrace();
				}
			}
		}
		
		// IF GATE RELATION
    		else if (annotationOrRelation.equalsIgnoreCase("rel")) {
			RelationSet relSet = RelationSet.getRelations(doc.getAnnotations()); 
			stats[0] = relSet.getRelations(type).size();
                        System.out.println("Relation of type == "+ type +" is :" + relSet.getRelations(type).size());
			for(Relation r : relSet.getRelations(type)){
				//System.out.println(((FeatureMap)r.getUserData()).get("chemical"));
				//System.out.println(((FeatureMap)r.getUserData()));
				
				FeatureMap features = (FeatureMap)r.getUserData();
				Map<String,String> normalizedfeatureMap = normalizedFeatureMap(doc,features);
				if (textBuffWriter != null){
					String line = "";
					line += doc.getName()+"\t"+r.getType()+"\t";
					for(Entry<String,String> e : normalizedfeatureMap.entrySet()){
						line += e.getValue()+"\t";
					}
					textBuffWriter.write(line+"\n");
				}
			}
	        
		} else {
			throw new RuntimeException("Unsupported type; Must be 'ann' or 'rel';");
		}
		
		stats[1]=featureValues.size();
		stats[2]=surfaceStrings.size();

		//statistics.put(doc.getName(), stats);
		return stats;
	}


	private static Map normalizedFeatureMap(gate.Document doc, FeatureMap features) {
		Map<String,String> result = new TreeMap();
		for(Entry e : features.entrySet()){
			String feature = e.getKey().toString();
			if(feature.endsWith("ANN") || e.getKey().toString().endsWith("Ann")){
				String nameOfFeature = feature.substring(0, feature.length()-3);
				Annotation ann = doc.getAnnotations().get((Integer)e.getValue());				
				normalizedFeatureMap(doc, ann.getFeatures(), nameOfFeature, result);				
			} else {
				result.put(e.getKey().toString(), e.getValue().toString());
			}
		}

		return result;
	}
	 
	private static void normalizedFeatureMap(gate.Document doc, FeatureMap features, String featureName, Map<String,String> result) {
		if(features.get("ENT_ID") != null){
			result.put(featureName, features.get("ENT_ID").toString());
			return;
		} else{
			for(Entry e : features.entrySet()){
				String feature = e.getKey().toString();
				if(feature.endsWith("ANN") || e.getKey().toString().endsWith("Ann")){
					String nameOfFeature = feature.substring(0, feature.length()-3);					
					Annotation ann = doc.getAnnotations().get((Integer)e.getValue());					
					normalizedFeatureMap(doc, ann.getFeatures(), nameOfFeature, result);
				}
			}
		}
		
	}
	

}
