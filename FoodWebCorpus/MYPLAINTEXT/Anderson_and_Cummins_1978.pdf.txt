PERSPECTIVES

Influencesof Diet on the Life Histories of Aquatic Insectsl'2
N. H. ANopnsoN ANDKnNNn,rs W. CuuuINs

J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

Departmentof Entomology and Departmentof Fisheriesand Lltildlife, OregonState University,
Corvallis, OR 97331, USA
ANoEnsoN,N. H., aNn K. W. CumrrNs.1979.Influencesof diet on the life historiesof aquatic
insects.J. Fish. Res. Board Can. 36:335-342.
Benthic speciesare partitioned into functional feeding groups based on food-acquiring
mechanisms.
Effectsof food quality on voltinism,growth rate, and sizeat maturity are demoncollectors,and scrapers.Food quality for
stratedfor representatives
ofgougersand shredders,
predators is uniformly high, but food quantity (prey density) obviously influencestheir life
histories.A food switch fiom herbivory to predation,or some ingestionof animal tissues,in
the later stagesis a feature of the life cycle of many aquatic insects.Temperatureinteracts with
both food quality and quantity in effectson growth as well as having a direct effect on control
of metabolism. Thus further elaboration of the role of food in life history phenomena will
require controlled field or laboratory studiesto partition the effectsof temperatureand food.
Key words: aquatic insects,feeding strategies,functional groups, life histories
ANoensoN.N. H.. ,lNo K. W. CulrurNs.1979.Influencesof diet on the life historiesof aquatic
insects.J. Fish. Res. Board Can. 36:335-342.
Nous avons s6par6les espbcesbenthiquesen groupes,selonIe type d'alimentationfoncde
tionnellefondd sur les mdcanismesd'acquisitionde la nourriture. Chez des reprdsentants
creuseurset de ddchireurs,de collecteurset de racleurs,nous ddmontronsles effetsde Ia qualit6
de la nourriture sur le voltinisme,le rythme de croissanceet la taille ir la maturitd. Pour les
prddateurs,Ia qualitd de la nourriture est uniformdmenthaute,mais la quantit6 de nourriture
(densit6desproies)influe dvidemmentsur leurscyclesbiologiques.Un changementde rdgime,
d'herbivore ir pr6dateur,ou une certaineabsorption de tissusanimaux aux stadesult6rieurs
est une caract6ristiquedu cycle oiologiquede plusieursinsectesaquatiques.ll y a interaction
de la tempdratureavecla qualit6 aussibien que la quantitd de nourriture dans sesell'etssur Ia
croissance,
de mOmequ'un effetdirectsur Ie contr6ledu mdtabolisme.Des dtudesplus pouss6es
sur le r6le de la nourriture dans les ph6nomdnesdu cycle biologiquen6cessiterontdes exp6riences de laboratoire ou des dtudes contr6l6es sur le terrain alin de s6parer les effets de la
tempdratureet de la nourritute.
ReceivedJuly 31, 1978
AcceptedDecember14, 1978

ReEule 31juillet 1978
Acceptdle 14 d6cembre1978

insects has been criticized (e.g. Resh 1976; Fuller and
Stewart 1977). However, the partitioning of taxa on
the basis of food-acquiring mechanisms, that is, community functional role, rather than what is eaten, has
proven useful in approaching certain ecological questions (Cummins 1973, 1974; Merritt and Cummins
1978; Wiggins and Mackay 1978). This approach,
which stressespartitioning of food resources on a community basis, reveals that detrital utilization by
shredders (coarse particle feeders) and collectors (flne
particle feeders) dominates most forested headwater
streams. This is in contrast to many terrestrial systems
in which the insects feed primarily on living plant
'Paper presentedat the Plenary Sessionof the 26th Antissue.
nual Meeting of the American BenthologicalSocietyheld at
Various aspectsof food (diet) as a factor influencing
Winnipeg, Man., May 10-12, 1978.
'Technical Paper No. 4875, Oregon Agricultural Experi- life histories, or life history strategies, are examined.
factor
ment Station, and Contribution No. 322 of the Coniferous Since fleld data implicating food as a causal
Forest Biome, EcosystemsAnalysis Studies, U.S. Interna- usually have other possible explanations, our task is
akin to completing a jigsaw puzzle with most of the
tional Biological Program.
pieces missing. Furthermore, the interaction of abiotic
factors, especially temperature, with food quality and
Printed in Canada (J5334)
quantity (food per unit of environment) confounds the
Imprim6 au Canada (I5334)

TnE study of trophic relations in freshwater communities reveals a bewildering array of foods available
and methods of resource utilization. As Hynes (1970,
p. 192) suggested "It may seem inappropriate to consider the food of invertebrates in any biotope, as it is
obviously as varied as the invertebrates themselves."
Because of such complexity, it seems necessary to attempt some simplification in evaluating the general
role of nutrition in controlling aquatic insect life histories - primarily growth (i.e. rate and maximum size
attained). The stereotyping of food habits, based on
what is eaten, at the generic or family level for aquatic

336

J. FISH. RES. BOARD CAN.. VOL. 36, 1979

controtot

7

-/t.EiliEilima-

per
Dens,tv

--

I

Unit of Food

tr\

I

/

;ill::'
Unrt ol Environment

controtof

Animar

t FoopauALrrI

"""01"""

J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

-\-

\ ^u,r,,'on",
Valus psr gn;1

""\\--

I

-

\

lT"""O@

'

t5

/
o"n.itvor
F o o dU n i t s

_L ;

Frc. 1. Relationshipbetweentemperatureand food in the
control of macroinvertebrate growth.

Jo-

,
G
ro€

problem (Fig. 1). A bias to lotic examples is evident
both in the experimental data presented and literature
cited because our research programs are directed to
stream studies.
Food Resources of Aquatic Insects
The range of organic materials potentially available
as foods for aquatic insects extends from the highly
refractory to the readily assimilable. The relative nutritional gradient is perhaps: (1) wood; (2) terrestrial
leaf litter; (3) fine particulate organic matter (FPOM);
(4) decomposing vascular hydrophytes and filamentous
algae; (5) living algae, especially diatoms; and (6)
animal tissues. Considerable evidence has accumulated
(e,g. Bdrlocher and Kendrick 1973a, b) that the
microbial flora associated with categories 1-4 is the
primary source of nutrition for aquatic insects. Monk
(1976) demonstrated a weak cellulase activity in gut
extracts of several aquatic insects but found no correlation between the occurrence of cellulase and the
abundance of cellulose in the diet. However, Tipula
and other shredders have been shown to have hind gut
symbionts that may be instrumental in the assimilation
of products of cellulose degradation (Meitz 1975).

Frc. 2. Life cycles of the wood gouger Hetero-plrrtron
calit'ornicum and the leaf shredder Lepidostoma quercina,
expressedas mean individual weight at each sampleinterval
at Berry Creek, Benton Co., Oreg. Water temperature is
given as mean monthly temperature.

rates to provide sufficient assimilative gut contact with
the microbial flora to meet the animal's nutrient requirements; (3) supplemental feeding on higher quality
food (especially the periphyton film); or (4) some combination of the above.
Examples of xylophages include the elmid beetle,
Lara avara, the calamoceratid caddisfly, Heteroplectron
californicum, the craneflies, Lipsothrix nigrilinea and
L. f enderi, and midges of the genus Brillia. Life cycle
studies of the dipterans are incomplete. Lara avara and
H. calilornicum both grow slowly; the former probably
requires 3 yr or more to complete development and the
latter has a 2-yr life cycle. Microbial symbionts capable
of digesting cellulose have not been demonstrated in
the hind guts of either species.
The growth pattern of H. calilornicum is compared
with that of a typical leaf shredder, Lepidostoma
quercina, in the same stream (Fig. 2). The major
Gougers and Shredders
growth period for both species is from September to
These functional groups encompass those speciesthat February when water temperature ranges from about
feed on coarse particulate organic matter (CPOM)
12 to 4'C. Lepidostoma quercina is univoltine. with
(food categories 1 and 2) and reduce the particle size an instantaneous growth rate (IGR) of 2.7o/o'd-1,
by chewing-gouging and mining wood, and skeletoniz- whereas H. calilornicum has a 2-yr cycle with an IGR
ing leaf litter. Woody detritus, the most refractory
of O.9Vo.d-t.
material in aquatic habitats, has a processing time of
The degradation of deciduous leaves by shredders has
at least decades. In water, microbial degradation of received considerable attention in the last decade and
wood occurs largely on exposed surfaces (Anderson was recently reviewed by Anderson and Sedell (1979).
and Sedell 1979). Initially food is available to in- From the viewpoint of dietary eifects on life histories,
vertebrates only through gouging at the surface, but some salient features of the shredder function are
when tissue softens, the inner tissues are mined. Al1. Life cycles of many shredders (e.g. Tipula and
though wood debris offers an extremely nitrogen-poor
many caddisflies) are keyed to the autumnal pulse of
diet (C to N ratio 300-500:1), in western coniferous leaf fall, with the major growth period occuring in the
forests it supports a considerable fauna (Anderson et late autumn and winter.
al. 1978). Life-history strategiessuggestedfor species
2. Shredders selectively feed on the leaves maximally
that exploit wood as a food source were ( 1) long tife colonized (conditioned) by microorganisms, especially
cycles with low metabolic activity; (2) high ingestion aquatic hyphomycete fungi. This is shown by the posi-

JJ I

PERSPECTIVES

J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

tive correlation between selective feeding and density
of microbial flora (Bdrlocher and Kendrick 1973a, b),
nitrogen content (Iversen 1974), and respiration rate
and ATP content per unit weight of leaf (M. Ward,
R. Speaker, and K. Cummins unpublished data). Thus,
the rate of leaf degradation (e.g. Alnus, Tilia, Fraxinus,
fast; Carya, intermediate; Quercus, Fagus and conifers,
slow) is a good predictor of shredder feeding
preference.
3. Although shredders selectively feed on the best
conditioned substrate, they can make an adjustment in
feeding rate to maintain their growth rate.
Fooo Querrry
magnifica

!,m
5"o

f.o

: ,oo

560

5oF Alnus Lnevrs ron Clistoronia

Compared with many common riparian species,
Alnus leaves are high in nitrogen, rapidly conditioned,
and one of the most palatable species for benthic invertebrates (Iversen 1974; Otto 1974; Petersen and
Cummins 1974; Anderson and Grafius 1975). Therefore, alder leaves have been used as a standard for
comparison in short-term growth studies of various
Trichoptera at Oregon State University. However, when
the limnephilid C. magnifica was reared through the
entire larval stage and compared with field collected
specimens, it became apparent that laboratory-conditioned alder leaves were inadequate to produce normal
growth. A diet including a supplement of wheat grains
or enchytraeid worms in addition to alder leaves has
enabled continuous rearing of C. magnifica (Anderson 1976, 1978). Feeding experiments with individuals
from the laboratory culture indicate that development
time was at least 10wk longer and mortality more
than twice as high on alder leaves compared with a
supplemented diet (Fig. 3). Mean pupal weight was
only 22.65 (!1.04) mg on the nonsupplementeddiet as
compared with 37.90 (-t-2.32) mg on the leaves plus
supplement.
Laboratory-conditioned leaves were compared as
food with leaves incubated in a stream to test the possibility that an inferior microbial flora developed on
laboratory-conditioned leaves. Conditioning time was
2-3wk at l2-15'C in the field and at l5'C in the
laboratory. Fecal production was greater with the
stream-conditioned leaves than for the laboratory-conditioned leaves (Table 1), which suggeststhat the former
were more palatable. On the evidence of their development to fifth instar and on final weights, larvae fed
on field-conditioned leaves were judged to be marginally more advanced than those fed on laboratoryconditioned leaves. However, the differences in weights
are not significant (P > .05), and both are markedly
below the weights of larvae receiving a wheat supplement. Low fecal production by control larvae is attributed to higher assimilation of the wheat diet. Thus,
laboratory conditioning is apparently not a major factor
in reduced growth rates on the nonsupplemented
detritus diet.

t^^

WEEXS

Frc. 3. Rate of development and survival of Clistoronia
magnifica larvae reared on Alnus leaves compared with a
control of Alnus plus a supplement of wheat grains and
enchytraeid worms. Both series reared at 15'C, with
conifer needles and sand for case material'
Higher quality food is required during the final instar
when growth is rapid and fat reserves are being laid
down prior to pupation. A series of 26 C. magnifica
larvae that had been reared from week 12 to 17 on
alder leaves were split into two groups after 5OVo of
the individuals had molted to the final instar. One group
was continued on alder leaves while the other received
leaves and a supplement of wheat plus worms. Mean
time to pupation was a further 17 wk for the alder
group as opposed to only 7 wk for those that received
the supplement. Even more striking were the differences
15.0
in survival and pupal weight: alder, (n : 4), i:
l1), f : 39.8mg. Pupae from
mg; supplement(n:
the stock culture (fed a supplement diet) averaged 40.tr
mg. Thus, the addition of the supplement for the final
5-7 wk of the larval stage provided suffcient nutriment
to allow normal weight to be regained.
Fooo Querrry oF BAsswooo (Tilia) AND HIcKoRY
( C ary a) Lravns FoR T i p ul a abd o minali s
Recent work at the Kellogg Biological Station (M.
Ward, R. Speaker, and K. Cummins unpublished data)
has shown that the shredder, T. abdominalrs, can increase its feeding rate to compensate for poor food
quality and thus maintain a typical growth rate. Two
levels of food quality were obtained by using hickory
and basswood leaves. These were incubated with
natural stream microorganisms for 9 wk at 10"C. Respiration rates were 3OVohigher for the basswood leaves,
indicating greater microbial conditioning than for
hickory and, consequently, a higher food qluality. Tipula
abdominalis larvae grew at the same rate in a 1-wk
experiment when fed either type of leaf separately. The
weight loss of the hickory leaves was about 304o
greater than the basswood, but the efficiency of food

J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

338

J. FISH. RES. BOARD

CAN., VOL. 36. 1979

Tlnln l. Growth, development,and fecal production of of high protein (animal) food may be required for inClistoroniamagnifca larvae rearedon laboratory-conditioned dividuals to achieve an appropriate mature weight.
Alras leayesQt : 40), field-conditionedAlnusleaves(n : 40),
and a control of leavesplus wheat grains (n : 20). Larvae
Collectors
rearedfor l8 d at 15"C.
In contrast to shredders that feed on CPOM that is
t-ab
Field
colonized by microbes both on the surface and throughleaves
leaves
Control
out the matrix, collectors utilize fine particulate organic
(FPOM) that is primarily surface-colonized by
matter
l t ( m g ) 1 0 . 5 6 + l . 1 l a1 0 . 5 6 + 1 . 1 1 1 0 . 5 6 + 1 . 1 1
i l n i t i aw
x F i n a l w t ( m g ) 1 1. 4 0 + 0 . 7 7 1 2 . 2 4 + 0 . 6 3 1 9 . 7 5 + 3 . 6 7 bacteria. The sources of FPOM food and its nutritional
quality varies considerably. Much of it is fecal material
68
82
95
% molted
produced by shredders and other functional groups that
Feces(mg/mg.d)
0.31
0.46
o.27
is recycled or "spiralled" (Wallace et al. 1977) by the
a95ft confidenceinterval.
stream biota. Other sources are wood and leaf fragments produced by physical abrasion and microbial
conversion to growth (relative growth rate/consumpmaceration, senescent periphytic algae, planktonic
tion) (Waldbauer 1968) for T. abdominalis larvae on algae, aquatic macrophyte fragments, small animals,
basswood was twice that on hickory. That is, the larvae and particles formed by the flocculation of dissolved
ingested hickory leaves more rapidly, compensating for
organic matter. The organic layer on stones, described
the lower food quality.
by Madsen (19'12) as a matrix of bacteria, extracellular
Feeding by T. abdominalis larvae was minimal on material, fungi, and organic and inorganic particles,
both basswood and hickory leaves when the conditionmay be used by collectorsor by scrapers(see below).
ing time was reduced to 2 or 3 wk. With only this type
Black fly larvae typify collectors that filter FPOM
of food available many of the larvae attempted to from the water column. Gut filling times are about 20crawl out of the experimental chambers and those that 30 min (Ladle et al. 1972). Since the food is passed
remained did not grow.
so rapidly, the nutritional value is probably derived by
The compensatory effect of food quality in over- stripping bacteria from the refractory detritus particles.
riding direct temperature effects on shredder growth Carlsson et al. (1977) have documented a relationship
is shown in Table 2. Basswood leaves conditioned at between the type of food available and the life cycle of
5oC were of higher quality than hickory leavesat 10"C. some Lapland black flies. The larvae at lake outfalls
When expressed per unit of temperature time (degree occur in denser aggregations and have faster growth
days), the growth rates of T. abdominalis and the than the same or other species occurring further downcaddisfly Pl,cnopsyche guttit'er were higher on bass- stream. Phytoplankton and coarse detritus () 2 pm)
wood than on hickory leaves.
occurred in similar amounts in all reaches, so they
The above experiments suggest that the overall concluded that small particles, from 2 pm down to colshredder feeding strategy is selection of the highest loidal size, were the resource that maintained the huge
quality (greatest microbial biomass) food available in larval aggregations at the lake outlet. This material is
a given leaf accumulation, increased feeding rate if the produced by decomposition on the lake bottom in winbest available is not of sufficient qualitv to maintain ter and is washed into the river during ice melt.
growth in the normal range. and emigraiion if quality
The influence of food quality, as indicated by paror quantity is below some minimal level. Temperature ticle-associated respiration rate or ATP content, on the
control of the general metabolic rate of shredders is growth of the collector-gatherer midge, Paratendipes
compensated to some extent bv the temperature- albimanus, is shown in Fig. 4 and 5. All food materials
mediated effects on food quality. In addition, u, d"-orrwere wet sieved to fhe same particle size range before
strated for C. magnifica (Anderson l9].6), some intake use. The oak and hickory leaves had been conditioned
ft"t-E 2.. ,Comparisonof growth rates of two shredderspecieson high (basswood,Tilia americana)and medium (hickory,
Carva glabra) quality leaf litter at different temperatures.Experimenti conductedfor 28 d in experimentalstream channels
undercontrolledflow and normalphotoperiodconditions.
Relative growth rate (% body wt)
Leaf
type
Basswood
Hickory

Mean
temp

cio Loss/

('c)

Degree
days

deg
day

5
10

147
282

0.38
o.27

Respirationof
leaves
ppm O:/g DW.h-l

0.022
0. 0 1 3

Tipula abdominalis

per day per deg daya
2.22
3.51

o.424
0.348

aThe factoring out of temperaturein thesecalculationsassurnesthat growth is very low at 0'C.

Pycnopsyche guttifer

per day per degdaya
4.70
5.19

0 .895
0.515

339

PERSPECTIVES

o

Paratendipes

q

Porolendipes

olSimonus

Hor

o5
J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

rrlbimanus

Q6

Hi c k o r y

OO
al
U
G

H
5
do

Hickory

o
g

o
4
u@

s@
EO
<to
dci

o.o
-Q

Ook

r--q

t.O

t!

ol

3

i p u l o Fe c e s

=a

F_;l

;o

u6i

n

:

l. l
tl

:'l o
=
- oo
o

Feces

]o
o
9
oro

t_l

o

l-:J

-o

EP

No lu rol Defritus

o.ooo

fipulo

E
F!'

0.006

0.012

0.018

Noturol Detritus

qO
4

24

16

S U B S T R A T EA T P C O N T E N T
RESPIRAIION
R A T E P E R D E G R C EC
Frc.4. Relative growth rate (Waldbauer 1968) of Frc. 5. Relative growth rate of Paratendipes albimanus
Paratendipesalbimanus fed four diets (all sieved to the fed four diets compared with substrate ATP content as an
sameparticle size) comparedat three temperatures(10, 15, index of microbial biomass(aM ATP/mg AFDW). SeeFig.
20'C) on the basis of substraterespiration rate (plO,.mg
4 caption for additionaldetails.
AFDW-''h'.'C').
Modified from Ward and Cummins
(1978).
action of temperature and food on growth of G. nigrior
is shown in Table 3. The ratio of gross primary producin aerated batch cultures with aquatic hyphomycete
tion to community respiration (P/R) is a reflection of
fungi inocula for 2 wk. The leaves were dried, ground, the available algal food, and the data indicate a strong
and wet sieved to the appropriate size range. The ex- relationship between P/R and the final larval (prepupal)
periments were conducted at three temperatures (10, weights.
15, and 20"C). Each of the three points for each food
As shown by Gose (1970), the amount of accumutype represents a temperature in Fig. 4 and 5. Although
lated temperature can influence the number of generain all casesthe higher temperature produced the highest tions of a scraper species, and it probably affects the
growth rate on a given food, it is clear that food quality
maximum size attained by individuals in a given cohort.
outweighed the direct effect of temperature on larval There was a 20% greater accumulation of day degrees
growth.
in the third-order stream, which is undoubtedly a con-

Scrapers

Tr.nrE 3. Dry weights of Glossosomanigrior prepupae from

This functionalgroup is typifiedby glossosomatidsix sites in the Augusta Creek watershed,Kalamazoo and
caddisflies, representatives of the baetid, heptageneid,
and ephemerellid mayflies, and psephenid beetle larvae
(Merritt and Cummins 1978). Scrapers are dependent
primarily on autochthonous production as a food resource but their mode of feeding also results in ingestion
of detritus, as well as the organic layer on stones
(Madsen 1972). The nutritional content of live algal
cells is high; Cummins and Wuycheck (1971) report
differences of several hundred calories per gram between diatoms and detritus.
Previous studies indicated that G/ossosoma nigrior
from first-order streams were much smaller than those
in a third-order stream (Cummins 19'73, 1975). Gut
content analysis revealed that larvae in the small stream
consumed more detritus whereas those in the thirdorder stream fed predominantly on diatoms. The inter-

Barry Co., Mich. (collectedNov. ll-12, 1977).

Stream
site

Stream
order

Smith Cr.
B. Ave.
Upper 43ro
Lower 43"d
C Ave.
Nagel's

I
1
2
3
J
-l

Degree
daysu P/Rb

2398 0 .3 9
0.73
2863 r . / o
3006

244 3

-c

2811

t:o

N

Mean
dry wt
(me)

C.V.
(%)

28
32
34
27
40
26

1.512
1. 6 6 6
4.018
4.638
3.749
4.507

13.1
12.3
17.7
2r.6
1 7. 3
11.5

aBasedon mean weekly temperatures.
bGross primary production community respiration measured over 24-hperiod in circulatingchambers(D. L. King and
K. W. Cumminsunpublisheddata).
cTemperatureregime approximately the same as Nagel's.

340

J. FISH. RES. BOARD

the nonfeeding prepupal stage is early autumn at the
time the colonial sponge declines and produces gemmules. Larvae that emerge later in the summer also
feed on sponge but only reach the third or fourth instar
before the sponges become inedible due to gemmulation. Thus, they are forced to shift to a detritus diet and
Predators
growth is suspended until spring when there is a return
sigPredators, that is, species that feed on live prey by to sponge feeding. Larvae of this cohort are
growth bethan
that
complete
smaller
those
nificantly
(75assimilation
efficiencies
active capture, have high
85Vo, Heiman and Knight 1975). Although food qual- fore overwintering. Resh indicates that the two cohorts
larval
ity is uniformly high, the quantity (prey density) can operate as independent functional units in the
gene flow between
vary significantly. Thus, effects of food on the life cycles stages,but presumably there is some
of predators occur as density e{Iects, both in numbers the two populations. Both cohorts have a univoltine
quality do not
or biomass of predators, or difierences in generation life cycle so the differences in food
of the seclarvae
greatly
the
duration.
However,
affect
time.
for
longer,
which exare
actively
feeding
cohort
ond
(1969)
populations
Azam and Anderson
compared
of Sialis calilornica in a stream section where chironomid poses them to more environmental hazards, and the
populations were high due to enrichment with sucrose smaller size of adults indicates a lower fecundity.
A shift from algal or detrital feeding to predation has
and urea, with an upstream nonenriched section. Sialis
been
demonstrated in later larval stages for several
calilornica was univoltime in the enriched area whereas
a portion of the population required 2 yr in the control stoneflies and caddisflies (Siegfried and Knight 1976;
section. Azam (1969) was able to simulate the growth Fuller and Stewart 1977; Winterbourn 1971; Wiggins
patterns that resulted in 1- and 2-yr life cycles by using 1977). The more unusual switch from predation to
heavy algal feeding was reported for the stoneflies,
different levels of rations in feeding experiments.
(1973)
Johnson
demonstrated with damselfly larvae Stenoperla prasina (Winterbourn 1974) and Acroneuria
that searching movement increases as prey density falls calilornica (Siegfried and Knight 1976); in both infrom high to low, but at very low prey densities the stances this was apparently a response to an alternative
larvae remain stationary and again wait for prey. pre- food that was available for a limited period. As pointed
sumably becauseof the high metabolic cost of increased out previously. the ingestion of animal tissue in the
later stages of the growth cycle may be an important
searching.
feature of the life cycle of many species of aquatic
Inter- and intraspecific competition and territoriality
were important factors affecting the life cycle of the insects.
damselfly, Pyrrhosoma nymphula, in Macan's (1977)
Discussion
20-yr study of trophic relations in Hodson's Tarn, England. Larvae of P. nymphula feed primarily on plankDifferences in growth rate directly affect duration of
tonic Crustacea as a "sit and wait" type of predator. life cyles (i.e. voltinism), size attained at maturity (and
When prey are scarce, P. nymphula can fast for long therefore fecundity, which is positively correlated with
periods. They may take two full summers to complete female size), and survivorship. Temperature and food
development if they do not obtain enough food to com- quality and quantity are control parameters with sigplete it in one. When predator density was high, there nificant interactions on rate of growth. Thus isolation
were two size-groups at the end of the summer. Macan of the role of food in life histories is difficult to achieve
suggests that the large larvae had obtained vantage without controlled laboratory or fleld experiments. As
points where food was frequently within reach, while
pointed out by Pritchard (1979), the duration and
the small larvae were relegated to inferior feeding sites. timing of aquatic insects life cycles are more indeterHe proposed that the starvelings would eventually die minate than is usually suggested.He cites examples of
unless good feeding sites became vacant. When fish cohort splitting, where individuals of the same cohort
were in the pond, their predation on large larvae would
may have l, 2-, or even 3-yr life cycles. Detrimental
lead to vacancies in optimal feeding sites which would
effects of a poor diet will be most apparent with multithen be filled by small larvae. Macan postulates a self- voltine species because those that have a univoltine (or
regulating population mechanism whereby a substantial longer) cycle usually have a period of arrested developconsumption of large larvae by fish does not greatly ment of variable length that can be shortened to inreduce the numbers of damselflies reaching maturity
crease the feeding interval (Pritchard 1976).
because the loss of large larvae allows the small larvae
The recent work of Sweeney and Vannote (1978)
to exploit the copepod population.
has convincingly shown that adult body size and
Resh (1976) demonstrated that food quality affected fecundity of a number of aquatic insects depend largely
the life cycle of the sponge-feeding leptocerid caddisfly, on thermal conditions during the larval period. Changes
Ceraclea transversa. Larvae that occur during the sum- of 2 or 3 deg Celsius either warmer or cooler than the
mer feed on the sponge, Spongilla lacustris, and reach optimum temperature regime can affect both the rate

tributing factor to the larger size of the G. nigrior larvae,
but the data also implicate food quality (e.g. ratio of
algal cells to detritus) and quantity (e.g. density of
algae per unit of rock surface).

J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

CAN,, VOL. 36, 1979

PERSPECTIVES

J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

and duration of larval growth to the extent that adult
size is significantly reduced. Thus, in any study of
growth, it is important to separate as far as possible
the direct (animal metabolism) and indirect (food quality and quantity) effects of temperature.
Acknowledgments

This researchwas supported by National ScienceFoundation Grant No. DEB 74-20744 A06 to Oregon State University, and Environmental SciencesSection, U.S. Department of Energy, Grant No. EY-76-S-02-2002to K. W.
Cummins at Michigan StateUniversity.
AuornsoN, N. H. 1976. Carnivory by an aquatic detritivore,
Clistoronia magnifca (Trichoptera : Limnephilidae). Ecology 57: 1080-1085.
1978.Continuousrearingof the limnephilidcaddisfly,
Clistoroniamagnifca (Banks),p.317-329. Proc. 2nd Int.
Symp. on Trichoptera, Reading, England. W. Junk, Tbe
Hague. 344p.
ANornsoN. N. H.. nNo E. GnArrus. 1975. Utilization and
processingof allochthonous material by stream Trichoptera. Int. Ver. Theor. Ansew. Limnol. Verh. 19: 30223028.
ANornsoN,N. H., e,NoJ. R. Snonrl. 1979.Detritus processing
by macroinvertebrales
in streamecosystems.
Annu. RevI
Entomol.24:351-377.
ANornsoN.N. H.. J. R. Snonr-r.L. M. Rosrnrs. aNo F. J.
TnIsrl. 1978. The role of aouatic invertebratesin orocessingof wood debris in coniferousforest streams.-Am.
Midl. Nat. 100:64-82.
Aze,lra,K. M. 1969. Life historv and oroduction studies of
Sialis californicr: Banks and Sialisioturdo Banks (Megaloptera: Sialidae). Ph.D. thesis, Oregon State Univ.
Corvallis,Ore. 111p.
Azan, K. M., aNo N. H. ANopnsoN.1969.Life history and
habits of Sialis rotunda and S. californica in western
Oregon.Ann. Entomol. Soc.Am. 62:549-558.
BAnlocnrn. F.. ,c.NnB. KnNonrcr. 1973a.Funei and food
preferencesof Gammaruspseuclolimnaeus.
Aich. Hydrobiol.72:501-516.
1973b.Funei in the diet of Gammaruspseudolimnaeus
(Amphipoda).O-ikos24: 295-300.
CnnLssoN,M., L. M. Nrr,ssoN,
B. SvrNssoN,S. Ulfstrand,nNo
R. S. WorroN. 1977.Lacustrinesestonand other factors
influencing the black flies (Diptera: Simuliidae) inhabiting
lake outletsin SwedishLaoland. Oikos 29: 229-238.
CuuutNs, K. W. 1973.Trophic relations of aquatic insects.
Annu. Rev. Entomol. 18: 183-206.
1974. Structure and function of stream ecosystems.
Bioscience24: 631-641.
1975.Macroinvertebrates,
p. 170-198.1z B. Whitten
[ed.] River ecology. Blackwell. Scientific Publications,
Oxford, England. 725 p.
CuuvtNs, K. W., aNo J. C. WuvcnEcr. 1971.Calorificequivalents for investigationsin ecologicalenergetics.Mitt. Int.
Ver. Theor. Ansew. Limnol. 18: 1-158.
Fur-r-En.
R. L.. aNo k. W. Srnwanr. 1977.'thefood habits of
stoneflies (Plecoptera) in the upper Gunnison River,
Colorado. Environ. Entomol. 6: 293-302.
Gosr, K. 1970.Life history and instar analysisof Stenophylax
(Trichoptera).Jpn. J. Limnol. 3l: 96-106.
griseipennis
HnrunN. D. R., lNo A. W. KNrcur. 1975.The influenceof
temperatureon the bioenergeticsof the carnivorous stone-

341

fly nymph, Acroneuria californica Banks (Plecoptera:
Perlidae)Ecology 56: 105-116.
HvNEs,H. B. N. 1970.The ecologyof running waters.Univ.
Toronto Press,Toronto, Ont. 555p.
IvEnsnN,T. M. 1974. Ingestion and growth in Sericostoma
personatum (Trichoptera) in relation to the nitrogen
content of ingestedleaves. Oikos 25; 278-282.
JonNson, D. M. 1973. Predation by damselfly naiads on
cladoceranpopulations: fluctuating intensity. Ecology 54:
251-268.
Lrolr, M., J. A. B. Bnss,aNo W. R. JnNrrNs.1972.Studieson
production and food consumption by the larval Simuliidae
(Diptera) of a chalk stream. Hydrobiologia 39: 429-448.
Mnca.N, "f . T. 1977.The influence of predation on the composition of freshwater animal contmunities. Biol. Rev.
Camb.Philos.Soc.52:45-70.
MaosEN,B. L. 1972.Detritus on stonesin streams.Mem. Ist.
Ital. Idrobiol. 29: 385-403.
Mrtrz, A. K. 1975.Alimentary tract microbiota of aquatic
invertebrates. M.S. thesis, Michigan State Univ., East
Lansing,Mich. 64 p.
Mrnnrrr, R. W., a.NoK. W. CuuvrNs [ed.]. 1978.An introduction to the aquatic insectsof North America. KendallHunt, Dubuque,Iowa, 441p.
MoNr, D. C. 1916.The distribution of cellulasein freshwater
invertebratesof different feeding habits. Freshw. Biol. 6:
471-475.
Orro, C. 1974.Growth and energeticsin a larval population
of Potamophylax cingulatus (Steph.) (Trichoptera) in a
south Swedishstream.J. Anim. Ecol. 43:339-361.
1974.Leaf processing
PErr,nsrN,R. C., aNn K. W. CururraINs.
in a woodland stream.Freshw.Biol. 4: 343-368.
Pnrrcnnno, G. 1976.Growth and development of larvae and
adults of Tipula sacra Alexander (Insecta: Diptera) in a
series of abandoned beaver ponds. Can. J. Zool. 542
266-284.
1.979. The study of dynamics of populations of
aquatic insects: The problem of variability in life history
exemplified by Tipula sacrc Alexander (Diptera: Tipulidae). Int. Ver. Theor. Angew. Limnol. Verh. 20. (In
press)
REsu, V. H. 1,976.Life histories of coexisting speciesof
Ceracleacaddisflies(Trichoptera: Leptoceridae): the operation of independent functional units in a stream
ecosystem.Can. Entomol. 108: 1303-1318.
Srncrnrro,C. A., aNo A. W. KNtcHr.1976.Prey selectionby
a setipalpian stonefly nymph, Acroneuria (Calineuria)
californica Banks (Plecoptera: Perlidae). Ecology 57:
603-608.
SwrrNrv, B. W., lNo R. L. VnNNorr. 1978.Sizevariation and
the distribution of hemimetabolous aquatic insects: two
thermal equilibrium hypotheses.Science2ffi: 444-446.
Wnr-nnaurn, G. P. 1968. The consumption and utilization of
food by insects.Adv. InsectPhysiol.5;229-282.
Wr-Lncn, J. 8., J. R. WresrEn,aNo R. W. Woooru,l. 1977.
The role of filter feedersin flowing waters. Arch. Hydrobiol.79: 506-532.
1979. Effects of food
Wano, G. M., lNo K. W. Cur'lr.aIus.
quality on growth rate and life history of Paratendipes
albimanus (Meigen) (Diptera: Chironomidae). Ecology
(In press)
WrccrNs, G. B. 1977. Larvae of the North American caddisfly
genera(Trichoptera). Univ. Toronto Press,Toronto, Ont.
401 p.
WrccrNs,G. B., nNo R. J. Mlcrev. 1979.Somerelationships

342

J. FISH. RES. BOARD CAN., VOL. 36, 1979

J. Fish. Res. Bd. Can. Downloaded from www.nrcresearchpress.com by UNIV NEW BRUNSWICK on 04/08/16
For personal use only.

between systematics and trophic ecology in nearctic
aquatic insects, with special reference to Trichoptera.
Ecology(In press)
WrNtEnnounN,M. J. 1971.An ecologicalstudy of Banksiolct
crotchi Banks (Trichoptera, Phryganeidae) in Marion

Lake, British Columbia. Can. J. Zool.49;636-645.
1974.The life histories, trophic relations and production of Stenoperlaprasina (Plecoptera) and Deleatidium
sp. (Ephemeroptera)in a New Zealand river. Freshw.
Biol.4: 501-524.

Benthic Lite Histories: Summary and Future Needsl'2
T. F. Warnns
Departmentof Entomology,Fisheriesand Wildlife, Universityof Minnesota, St. Paul, MN 55108,USA
Wnrrns, T. F. 1979.Benthic life histories: summary and future needs.J. Fish. Res. Board
Can.36: 342-345.
The symposiumindicatedmany ways in which greaterknowledgeof benthiclife histories
can be used to developand improve techniquessuch as sampling,taxonomic methods,and
bioassays.Benthicorganisms'diet and physicalenvironment,factorsvariablein nature, were
shownto be capableof modifyingcertainlife historyfeaturessuchasgrowth rate and voltinism.
The lack of accumuiatedlife history data and the need to tailor sampling schedulesto life
history eventswere commonly identilied elementsin the symposium.Future researchneeds
included (1) basic data on benthic life history,(2) improved taxonomy of immature benthic
invertebrates,and (3) understandingthe entire life history of an organismin relation to the
seasonalprogressionof its environment. Managementimplications of benthic life history
information includedmore applicabledata from long-termbioassayson all life history stages,
and improved managementof streamfisheriesthrough habitat alteration to manipulate benthic
production.
Key words: life history, benthos, symposium
WarEns,T. F. 1979.Benthic life histories: summary and future needs.J. Fish. Res. Board
Can.36: 342-345.
Au cours du colloque,on a indiqud plusieursfagonsd'utiliser de meilleuresinformations
sur le cycle biologiqued'organismesbenthiquesafin de mettre au point et ameliorerles techniquestellesque l'dchantillonnage,
les m6thodestaxonomiqueset Ies analysesbiologiques.On
a d6montr6que le rdgimeaiimentaireet le milieu physiquedesorganismesbenthiques,facteurs
variables dans la nature, peuvent modi{ier certainescaract6ristiquesdu cycle biologique, telles
que le rythme de croissanceet le voltinisme.Parmi Iespoints commun6mentidentifidsau cours
du colloque,on note le manquede donn6esaccumul6essur les cyclesbiologiqueset Ie besoin
d'dchelonnerl'dchantillonnage selon les dvdnementsde ces cycles. Parmi les besoinsfuturs en
recherche,on compte: (1) les donndesde basesur le cycle biologiquebenthique,(2) une taxonomie am6liorde des invert6brds benthiques immatures et (3) la compr6hension du cycle
biologique complet d'un organismeen relation avec l'6volution saisonnibrede son milieu.
sur les
Pour ce qui est de la rnanidredont les gestionnairespeuventutiliser les connaissances
cycles biologiquesbenthiques,on note des rdsultatsplus facilement applicablesd'analyses
biologiquestr long terme sur tous les stadesdu cycle biologique.On note aussiune meilleure
gestion des pOchesfluviales par altdration de I'habitat et manipulation de la production de
benthos.
ReceivedJuly 31, 1978
Accepted December 14, 1978

Regule 31 juillet 1978
Accept6le 14 ddcembre1978

'Paper presented at the Plenary Session of the 26th Annual Meeting of the North American Benthological Society
held at Winnipeg, Man., May 10-12, 1978.
'Paper 10609, scientific journal series,Minnesota Agricultural Experiment Station, St. Paul, MN 55108,USA.
Printed in Canada (J5337)
Imprim6 au Canada (J5337)

