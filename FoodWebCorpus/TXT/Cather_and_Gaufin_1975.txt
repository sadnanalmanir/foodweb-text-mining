LIFE HISTORY AND ECOLOGY OF MEGARCYS SIGNATA (PLECOPTERA: PERLODIDAE), MILL CREEK, WASATCH
MOUNTAINS, UlAH^

Mary R. Gather- and Arden R. Gaufin-

—Abstract. During an investigation of some of the stoneflies (Plecoptera) of Mill Creek, Wasatch
Mountains, Utah, Megarcys signata, a large omnivorous stonefly, was found to have a univoltine life
history and a slow seasonal life cycle.
Temperature appears to affect the growth rate of Megarcys signata. Warmer stream temperatures accompany the acceleration of the growth rate, whereas cooler stream temperatures apparently retard
the growth rate.
Periods of maximum absolute growth rate con-espond with maximum carnivorous feeding from August to September and March to April. Ghironomidae, Ephemeroptera, and Plecoptera, in that order, were the most abundant prey in the foreguts. Young n^-mphs ingested considerable amounts of diatoms, filamentous algae, and detritus but not as much animal matter as did older nymphs.
Megarcys signata was unifonnly distributed throughout Mill Creek, except at the lowest station, where few nymphs were found.
Emergence occurred in May and June, the peak occurring in June. The mean size of females
and males decreased as emergence progressed.

This report is part of an eighteen-month study of some of the stoneflies of Mill Creek, Wasatch Mountains, Utah. Be-
cause a detailed description of the study
area and the methods and materials is
given in another paper (Cather and Gaufin 1975, only a summary is included
here.
Mill Creek Canyon is located 11 km
southeast of Salt Lake City, Utah, in the Wasatch Mountains of the Middle Rocky Mountain Province. Six stations were se-
lected along a 12 km length of the stream
in the Wasatch National Forest with elevations ranging from 1,605 to 2,280 m. The sampling stations were nimibered
consecutively. Station I denoting the high-
est elevation. The three lower stations (1,605-1,785 m) are easily accessible all
year, but the three upper stations (1,995-
2,280 m) are accessible only in the simimer and fall. Average minimum and maximum daily flows were 0.3 m^/sec and 1.2 mVsec, respectively, during the study period. Depth averaged 11-45 cm,
and current averaged 0.2-0.7 m/sec during the fall when measurements were taken. The substrate of the sampling area at all stations ranged from coarse sand to small
cobbles. Minimum water and air tempera-
tures recorded during adult emergence
were 3 C and 9 C, respectively. Maximum
water and air temperatures during this period were 13 C and 26 C, respectively. Water chemistry was similar at all sta-
tions. Dissolved oxygen ranged from 6.0 to 8.5 mg/1 (70-120 percent saturation).

calcium bicarbonate 109-189 mg/1, calci-
vmi carbonate 0-2.4 mg/1, pH 7.5-8.3,
total hardness 100-340 mg/1, and conductivity 312-859 mhos/cm.
Methods and Materials
Nymphs of Megarcy signata were col-
lected at least monthly from June 1971 to December 1972 at each of six stations. Additional nymphs were collected in spring 1973 for food habit studies. Handscreens of mesh sizes 7 and 9 sq/cm were used, the smaller handscreen being used during the majority of the study in an
Anattempt to collect the smaller instars.
area of about 80 cm- of the stream bottom was disturbed in an attempt to collect at least 100 nymphs monthly. All nymphs were preserved in 80 percent ethanol.
Adults were collected weekly throughout the emergence period and biweekly during peak emergence using a sweep net and handpicking from vegetation, rocks, and bridges. All adults were preserved in 80 percent ethanol.
The interocular distance of all nymphs and adults was measured to the nearest
mm0.1 using an ocular micrometer in a
dissecting microscope for determining
growth rates and to see if the mean size of
the adults decreased as emergence progressed. The nymphs were identified as males and females when possible.
Foregut analyses were conducted on 200
nymphs collected from the field. Nymphs
were selected from an upper (I) and a

^Study supported by Environmental Protection Agency Traineesliip Grant No. 5T2-WP-542-03. ^Department of Biology, University of Utah, Salt Lake City.

39

40

GREAT BASIN NATURALIST

Vol. 35, No. 1

lower (IV) station during each season
V(Station had to be substituted for Sta-
tion I during winter and spring). The
method used followed Swapp (1972). All

prey animals were enumerated and identi-

fied to order except where family or generic determination was possible. Three

types of the family Chironomidae were

recognized and designated as species a, b,

and c (based on morphology of head

capsule). These are discussed under re-

sults and discussion but are not separated in Table 1. All algae were determined to genus where possible. Percentage compo-

sitions of algae and detritus were esti-

mated when present, and dominant items

Awere recorded.

volume analysis was

not conducted.
Identification of nymphs and adults fol-

lowed Gaufin et al. (1966), and nomenclature followed lilies (1966) and Zwick
(1973).

Results and Discussion

The only systellognathan stonefly pres-

ent in Mill Creek in numbers large enough

for analysis is Megarcys signata. This

species exhibits a slow seasonal type of

life cycle. Emergence and oviposition oc-

cur in May and June with hatching soon

mmafter. Small nymphs (0.5

interocular

distance) appear in July at the lower sta-

tions. Nymphs of comparable size gen-

erally appear for the first time in August,

September, and October at the upstream

stations. Rapid growth occurs from Aug-

ust to emergence (Figs, la, b, c). The size

lOOn

75

50-

25

2.5-
-. 2.0E
o
].o^
0.5

Month
Fig. 1. Growth of Megarcys signata. Arrows indicate emergence. I. O. indicates interocular distance: (a) Monthly mean size as a percentage of total mean size at Station IV; (b) Monthly mean cumulative growth at Station IV; (c) Monthly mean absolute growth rate (data pooled from all stations). A indicates change in I. O. distance.

March 1975

GATHER, GAUFIN: UTAH PLECOPTERA

41

frequencies of nymphs and the mean
cumulative growth at each station are shown in Figs. 2 and 3, respectively.
A comparison of the cumulative growth
at all stations reveals that the most rapid
growth occurs at the lowest (warmest) stations (Fig. 4). Thus, there seems to be a direct correlation between growth and
temperature. Baumann (1967) found no
direct correlation in this species in Mill Creek. Seasonally the most rapid growth
occurs during the fall and early winter

(September-January). Growth apparently slows, but does not stop, during the winter (January-March), increases from March to April, and then decreases from April to May, prior to emergence. Sheldon (1972) reported similar results in his study on the Arcynopteryx species complex he studied in California. However, Schwarz (1970) reported no growth at times during the winter in other Systellognatha. The correlation between growth and food habits will
be discussed later.

2.0'
101

103 2 A

34 27

JII
O 1,0

4 3

2.0-

33

•l 'h

1.0-
50

MAM ASO30 NDJ A

JF

JJ

"n ^

Month

Sfa IV

2.0-|

b

1.0-1

1 2.0-
O 1.0-

38 33

AC 15 9 1

r<^h°^n

^^

ly ,3 1/

10 5 'l'^

96

? cf:

23

10'

J A S5S 5]

MF A ?ir

'
I
50
56N F

Month

Fig. 2. [Frequency distribution of nymphal size classes of M. signata. Number of individuals shown above each polygon; males and females as indicated: (a) Stations I, II, and III; (b) Stations
IV, V, and VI.

r

42

GREAT BASIN NATURALIST

Vol. 35, No. 1

Megarcys signata seems to show no preference for either the upstream or downstream stations. This species is evenly distributed throughout the stream except at Station VI. Here the substrate is almost entirely cemented. Where the water is deep enough for this large insect, the current is too slow; where the current is fast enough, the water is too shallow and the substrate too homogeneous. More individuals were collected at Station IV
than at any other station.

The emergence of M. signata began in
early May at the lower stations and lasted
until late June at the higher stations (Fig.
5). Peak emergence was in June. Bau-
mann (1967) found this species emerg-
ing from late April to mid-July in Mill
Creek. Emergence is progressively later as the elevation increases. Baumann (1967), Hynes (1970), and Nebeker (1971) reported similar results. They
Vwere first collected at Stations III and
in early May, when the water tempera-

Sta

2.0-
--HH

O ).0-
2.0-
ASONDJ FMAMJ JASOND
J
Month
Sta 2.0-
1.0-

o - la

T~y ~r~T

T—

Fig. 3. Mean cumulative growth of nymphs of M. signata. Vertical line represents size range of
nymphs; shaded area represents standard deviation; unshaded area represents standard eiTor of the mean; solid line connects means; rectangle represents emergence period: (a) Stations I, II, and III;
(b) Stations IV, V, and VI.

March 1975
Ja

GATHER, GAUFIN: UTAH PLECOPTERA
100-

43

1 5"

^^

Fig. 4. Comparison of mean cumulative

growth of nymphs of (D) M. signata at Stations

I-V. Station I represented by (o); Station II by

();(•); Station III by

Station IV by (A);

VStation by (A); and Station VI by (D): (a)

July, 1971 through June, 1972; (b) July, 1972

through December, 1972.

tures were 3 C and 5 C, respectively.
Emergence ended in late May (water tem-
perature 9 C) at Station V, but lasted until mid-June (12 C) at Station IV and late June (5.5-7 C) at the three upper stations. Megarcys signata is a secretive insect that
hides in cracks under bridges or among vegetation to escape warm summer tem-
peratures. Clusters of these stoneflies
usually containing one female and several males were often collected in these hiding places. Brinck (1949) reported the same phenomenon in related species. The collection data probably reflect this secretive habit in that M. signata should have been
collected earlier at Station IV.
Females and males generally emerged together in a 1:1 ratio. Harper and Pilon (1970) reported similar findings. Only 55

44

GREAT BASIN NATURALIST

Vol. 35, No. 1

(1972) reported a similar phenomenon.

The mean size of females decreased from

mm2.4 to 2.3 nun, while that of males

mm mmdecreased from 1.9

to 1.8

(Fig.

7). The data were rather inconclusive

because only small numbers of adults were

collected in May. More intensive collecting may reveal this trend more strongly.

Sheldon (1972) reported similar results in

the Arcynopteryx species complex he

studied and suggested that the decrease in
mean size also influenced fecundity; that

is, the smaller females carried fewer eggs.
Warmer stream temperatures and increasing photoperiod may act as emer-

gence cues before growth and egg develop-

ment are completed (Khoo 1968, Hynes

1970).
A total of 200 nymphs of M. signata

were dissected and the foreguts examined

for a preliminary food-habit analysis. The

nymphs were selected from upper and lower stations and from each season of the
year. Table 1 gives the results of this analysis. Of those foreguts examined, 39 were empty (some of these stoneflies were beginning to molt; others had just molted). Chironomids seemed to be the preferred food item. Three types of chironomids based on differences in the head capsules were recognized. Of these three
types, one was much more frequently
Afound in the foreguts. total of 442 were
found in 39 percent of the insects ex-
amined. The other two types were found only occasionally. Mayflies were the second most abundant food item ingested (37
percent of all individuals). Baetis spp. were most frequently recognized. Stoneflies, notably chloroperlids, comprised a
considerable portion of the gut contents,
also (27 percent of the individuals). Rich-

T.\BLE 1. Percentage of dissected nymphs of Megarcys signata containing specific food items. Season

March 1975

gather, gaufin:

46

GREAT BASIN NATURALIST

Vol. 35, No. 1

Month
Fig. 7. Size range of adults of M. signata.
diatom frustules, leaf fragments, and other plant remains were found in 30 percent of the insects examined. Other diatoms en-
countered were Gomphonema spp., Cym-
bella spp., and Nitzschia spp. These are
common stream dwellers of Mill Creek,
occurring on the rocks in shallow water. There are some significant seasonal dif-
ferences in the food items found in the
guts. In the summer (July-early September) significantly more M. signata (58
percent) ingested mayflies than in the fall
at Station IV, but at Station I the numbers feeding on Ephemeroptera were low (5 percent). Stoneflies were found in only 9 percent of the guts examined at Stations I and IV. Chironomids, on the other hand, were present in 32 and 45 percent of the guts at Stations I and IV, respectively. The number of chironomid head capsules found was also greater than in the fall. At Station IV diatoms were notably lack-
ing in the gut contents, although mayflies and chironomids were present. This could be because the swift current dislodges the algae from the substrate and because the deeper water and suspended solids from spring runoff shut out sufficient light. However, at Station I, where the water was shallower, diatoms were found in the majority of the guts examined {Navicula

spp. were found in all stoneflies examined). Filamentous algae and detritus
were also present in many guts. Enteromorpha spp., a filamentous green algae,
was the dominant plant material in 14 per-
cent of the guts, while detritus was the dominant item in 77 percent of the guts.
This herbivorous material may have come
from the stomachs of the chironomid prey
(mayflies present in only 5 percent of the
stoneflies) since Oliver (1971) states that some chironomids feed on algae and
detritus. However, many guts contained
detritus as the dominant material without chironomids being present. It is conceiv-
able that during the summer the newly hatched nymphs of M. signata could feed
at least partially on diatoms, filamentous algae, and detritus. At this stage they are probably not as effective a predator as at
later life stages. Coffman, Cimmiins, and Wuycheck (1971) found a similar pattern
in other groups of insects, in that young individuals consumed primarily detritus but shifted to algal or animal ingestion as they matured. The lowest absolute growth rate occurs in the summer from July to August despite increased carnivorous feeding. Growth increases sharply from August to September, however.
In the fall (mid-October) at the two
stations analyzed, Ephemeroptera were found in 19 percent of the guts at Station IV and in none of the guts at Station I. This can probably be explained on the
basis of emergence of many of the mayfly species. One mayfly adult was found in-
gested, however. Plecoptera were also
relatively rare in the fall, many having emerged already. Many summer and fall emergers may still be in the egg stage or
too small to be prey. Chironomidae were least numerous at this time of year also
(present in only 17 percent of the insects
examined). Diatoms and detritus were the most numerous items found in the guts during the fall. Since M. signata emerges mostly in the summer, the fall specimens
represent some of the smaller sizes. They would not require as much food and thus
are adapted to the relative paucity of
prey species. At Station IV even diatoms except Navicula spp. were not numerous
in those stonefhes examined. The swift current could be a factor in removing many from the substrate. At this station more individuals had fed- on mayflies and
stoneflies than at Station I. Another difference was that more empty foreguts

March 1975

GATHER, GAUFIN: UTAH PLEGOPTERA

47

were found in those stoneflies dissected was present in 43 percent of the guts and

from Station IV (maximum number reached) than at Station I (9 vs. 2). The
lack of sufficient prey could be a limiting

comprised the dominant item in 29 per-
cent of those examined. The mmiber of empty foreguts increased to a maximum

factor. However, the period October- again during this time.

November represents one of the highest

The early spring (March) was also a

absolute growth rates during the year. time of increased carnivorous feeding.

The growth rate data were obtained by pooling all samples from all stations, how-

This also coincides with a significant increase in the absolute growth rate. At this

ever.
During the winter (January-February) mayflies, stoneflies, caddisflies, and chi-
ronomids in the guts increased signifi-
cantly. Sheldon (1972) reported similar results in some of the Arcynopteryx spe-
cies complex he studied. In the winter at
Station IV, there were prey species present in more of the guts than at any other time during the year. This represents an increase in the absolute growth rate but a decrease from the previous month. The calculated absolute growth rates from November to December and from December to January are questionable, since only 3 M. signata nymphs were collected in December. This increase in growth rate is
probably correlated with the availabilit}^ of food, because only the family Capniidae are emerging. Chironomids were the most numerous, being present in 88 percent of the guts examined. These slower-moving insects would be easier prey than the fast-
er mayflies and stoneflies. Stoneflies also
increased dramatical!}^ in the guts despite
the fact that the numbers available decreased due to winter emergence. By winter, however, the summer and fall emerg-
ers, such as the family Chloroperlidae,
may have attained a sufficient size to be
suitable prey. Surprisingly, the increase in
numbers of prey found in the guts also coincides with one of the periods of the least amount of growth. The low winterstream temperature could stress the
nymphs enough that some growth may be
sacrificed even though an adequate food

time many stoneflies and mayflies are ap-

proaching their maximum size prior to

emergence. The quiescent stage just be-

fore emergence may also make them easi-

er prey. The percentage of mayflies and

stoneflies in the guts increased at Station

V but decreased at Station IV. One ex-

planation for this may be that during

spring runoff, the water level is deeper

at Station IV than at Station V. However,

the occurrence of chironomids decreased

Vsignificantly at Station

but remained

high at Station IV. The reason for this

was not determined. Some emergence

could have occurred at Station V, but be-

cause the stations are in close proximity

this would not seem to be the answer.

Sheldon (1972) stated that mature

nymphs of the Arcynopteryx species com-

plex he studied decrease their consumption of animal food in the spring (April). At

this time M. signata in Mill Creek under-

goes a noticeable decrease in absolute

growth rate prior to emergence. Diatoms

and other algae were still relatively rare

in the guts examined. Presumably spring runoff created spates which might have

removed these forms from the substrate.

Detritus was found in 85 percent of the

guts at Station V. Increased runoff due to

snow melt contributed large amounts of
allochthonous detritus to the stream. The greater depth of water at Station IV may

have prevented the concentration of detri-

tus and effectively removed it. On the

Vother hand, at Station

the channel is

wide enough that shallow areas are avail-

supply may be available. The number of
fliatoms and other algae decreased signifi-
(antly, probably due to the decreased
water temperature and lack of sufficient solar radiation. Nevertheless, Nayicula
spp. and Gomphoncma spp. were still nu-
merous. Enteromorpha spp. were dominant in 21 percent of the guts examined (luring this period. The stoneflies dissect-
V(h1 from Station were collected in Feb-
ruary, however, when stream temperatures were beginning to warm slightly and

able for detritus to collect.

In discussing food habits it is important

to remember that the partitioning of re-

sources is accomplished by the w^ide distri-

bution in size range, which decreases in-

traspecific competition (TTartland-Rowe,

1964; Radford and Hartland-Rowe, 1971).

In most samples a difference in inter-

ocular measurement of M. signata aver-

mmaged 0.6-1.0

from the smallest to

largest individuals. This was a significant-

ly larger size range than in the euholog-

solar radiation was increasing. Detritus nathan species studied.

48

GREAT BASIN NATURALIST

Vol. 35, No. 1

Acknowledgments
This study is part of a Ph.D. research
project by the senior author. The authors wdsh to thank Bill P. Stark for his helpful
suggestions.

Literature Cited

ABaumann, R. W. 1967.

study of the stone-

flies (Plecoptera) of the Wasatch Front, Utah.

Unpublished M.S. thesis, Univ. Utah, Salt

Lake City. 114 pp.

Brinck, p. 1949. Studies on Swedish stoneflies.

Opusc. Entomol. 11:1-250.

C.\THER, M. R.. AND A. R. G.\UFiN. 1975. Com-

parative ecologv of three Zapada species of

Mill Creek. Wasatch Mountains, Utah (Ple-

coptera: Nemouridae). Submitted to Amer.

Midi. Nat.
CoFFMAN, W. P., K. W. Cummins, and J. C. WuYCHECK. 1971. Energy flow in a wood-
land stream ecosystem. I. Tissue support
trophic structure of the autumnal communitv.

Arch. Hydrobiol. 68:232-276.

Fenneman, N. M. 1931. Physiography of west-
ern United States. McGraw-Hill Book Co.,
New York. 534 pp.

Gaufin. a. R., a. V. Nebeker, and J. Sessions. 1966. The stoneflies of Utah. Univ. Utah

Biol. Ser. 14:1-93.

Harper, P. P., and J. G. Pilon. 1970. Annual patterns of emergence of some Quebec stone-

flies. Can. J. Zool. 48:681-694.

Hartland-Rowe, R. 1964. Factors influencing the life histories of some stream insects in Alberta. Verb. Internat. Verein. Limnol. 15:

917-925.

Hynes, H. B. N. 1941. The taxonomy and ecology of the nymphs of British Plecoptera with notes on the adults and eggs. Proc. Roy. Entomol. Soc. London 91:459-557. . 1970. The ecology of stream insects. Ann. Rev. Entomol. 15:25-42.
Illies, J. 1966. 'Katalog der rezenten Plecoptera. Das Tierreich, 82. Walter de Gruyter and Co., Berlin. 632 pp.
Khoo, S. G. 1968. E.xperimental studies on dia-
pause in stoneflies. L Nymphs of Capnia bifrons (Newman). Proc. Roy. Entomol. Soc. London 43:40-48.
Nebeker, A. V. 1971. Effect of temperature at different altitudes on the emergence of aquatic insects fi-om a single stream. J. Kans. Entomol. Soc. 44:26-35.
Oliver, D. R. 1971. Life history of the Chironomidae. Ann. Rev. Entomol. 16:211-230.
Radford, D. S.. and R. Hartland-Rowe. 1971. The life cycles of some stream insects (Ephemeroptera, Plecoptera) in Alberta. Can. En
tomol. 103:609-617.
Richardson, J. W., and A. R. Gaufin. 1971. Food habits of some western stonefly nymphs. Trans. Amer. Entomol. Soc. 97:91-121.
ScHW.'^RZ, p. 1970. Autoekologische Untersuch-
ungen zum Legenszyklus von Setipalpia-Ar-
ten (Plecoptera). Arch. Hydrobiol. 67:103-140. Sheldon, A. L. 1972. Comparative ecology of
Arcynopteryx and Diura (Plecoptera) in a
California stream. Arch. Hydrobiol. 69:521-
546. Swapp, T. E. 1972. Food habits and life history
of stoneflies of the Cle Elum River, Washington. Unpublished M.S. thesis. East. Washing-
ton State Coll.. Bellingham. 58 pp. ZwicK, P. 1973. Insecta: Plecoptera. Phylo-
genetisches system und katalog. Das Tierreich, 94. Walter de Gioiyter and Co., Berlin. 465 pp.

